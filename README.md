# README #

### What is this repository for? ###

* This is a messenger for VK. It's called VK QM (VK Quick Messenger). Please keep in mind that I don't support this project anymore. The sources are available for anyone.
* Version 1.0

### How do I get set up? ###

You should have Open SSL binaries and Qt version 5.0 and higher. The last version was compiled using Visual Studio 2012. There is .sln file and .pro in repo as well. I don't think you'll get any problems with deploying.

### Who do I talk to? Feel free to contact!###
* VK - https://vk.com/arman.oganesyan
* Telegram - https://telegram.me/arman_oganesyan