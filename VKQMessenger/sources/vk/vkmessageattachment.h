#ifndef VKMESSAGEATTACHMENT_H
#define VKMESSAGEATTACHMENT_H

class VKMessageAttachment;

typedef QList<VKMessageAttachment> VKMessageAttachments;
typedef QMap<QString, VKMessageAttachments> VKMessageAttachmentsMap;

class VKMessageAttachment
{
public:	
    enum MessageAttachmentType
    {
        Audio,
        Photo,
        //Video,
		Unknown
    };

    VKMessageAttachment(MessageAttachmentType type, QString url, QVariant messageId = QVariant());
    //VKMessageAttachment(QVariantMap attachmentNode);

	static VKMessageAttachments parseAttachments(QVariantMap attachmentNode, QVariant messageId = QVariant());
	static MessageAttachmentType typeByName(const QString& name);
	static QStringList toStringList(const VKMessageAttachments& attachments);

	MessageAttachmentType type();
	QString url();
	QVariant messageId();


private:
    MessageAttachmentType m_type;
    QString m_url;
	QVariant m_messageId;
};

#endif // VKMESSAGEATTACHMENT_H
