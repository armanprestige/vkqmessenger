#ifndef VKUSER_H
#define VKUSER_H

class VKUser
{
public:
    VKUser();
    VKUser(QVariant id, QString fname,
           QString lname, QPixmap avatar,
           bool isOnline, QString status,
           bool onlineMobile);

    VKUser(const VKUser &T);

    bool operator < (const VKUser &T) const
    {
        return m_id.toUInt() < T.m_id.toUInt();
    }

    inline bool operator == (const VKUser &T)
    {
        return m_id.toUInt() == T.m_id.toUInt();
    }

    inline QPixmap& avatar() { return m_avatar; }
    inline QVariant id() const { return m_id; }
    inline QString name() const { return m_name; }
    inline QString status() const { return m_status; }
    inline bool isOnline() const { return m_isOnline; }
    inline QString fname() const { return m_fname; }
    inline QString lname() const { return m_lname; }
    inline bool onlineMobile() const { return m_onlineMobile; }

    void setAvatar(QPixmap& pix)
    {
        m_avatar = pix;
    }

    void setIsOnline(bool io)
    {
        m_isOnline = io;
    }

private:
    QVariant m_id;
    QString m_fname;
    QString m_lname;
    QString m_name; // fname+lname
    QPixmap m_avatar;
    bool m_isOnline;
    QString m_status;
    bool m_onlineMobile;

};

typedef QList<VKUser*> VKUserList;
typedef QMap<QString,VKUser*> VKUserNamedMap;

Q_DECLARE_METATYPE(VKUser)
Q_DECLARE_METATYPE(VKUser*)


#endif // VKUSER_H
