#include "stdafx.h"
#include "vkuser.h"

VKUser::VKUser()
{
}

VKUser::VKUser(QVariant id, QString fname, QString lname,
               QPixmap avatar, bool isOnline,
               QString status, bool onlineMobile)
{
    m_id = id;
    m_fname = fname;
    m_lname = lname;
    m_name = QString(m_fname + " " + m_lname);
    m_avatar = avatar;
    m_isOnline = isOnline;
    m_status = status;
    m_onlineMobile = onlineMobile;
}

VKUser::VKUser(const VKUser &T)
{
    m_id = T.m_id;
    m_fname = T.m_fname;
    m_lname = T.m_lname;
    m_name = QString(m_fname + " " + m_lname);
    m_avatar = T.m_avatar;
    m_isOnline = T.m_isOnline;
    m_status = T.m_status;
    m_onlineMobile = T.m_onlineMobile;
}
