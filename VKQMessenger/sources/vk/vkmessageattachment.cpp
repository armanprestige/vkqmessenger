#include "stdafx.h"
#include "vkmessageattachment.h"

VKMessageAttachment::VKMessageAttachment(MessageAttachmentType type, QString url, QVariant messageId)
{
	m_type = type;
	m_url = url;
	m_messageId = messageId;
}

/*static*/VKMessageAttachments VKMessageAttachment::parseAttachments(QVariantMap attachmentNode, QVariant messageId)
{
	if (attachmentNode.isEmpty())
		return VKMessageAttachments();

	VKMessageAttachments attachments;
	uint attachmentNumber = 1;

	forever
	{
		QString attachmentNodeNameUrl = QString("attach%0").arg(attachmentNumber);
		QString attachmentNodeNameType = attachmentNodeNameUrl + QString("_type");

		attachmentNumber++;

		if (!attachmentNode.contains(attachmentNodeNameType) ||
			!attachmentNode.contains(attachmentNodeNameUrl))
			return attachments;

		QString attachmentType = attachmentNode.value(attachmentNodeNameType).toString();
		QString attachmentUrl = attachmentNode.value(attachmentNodeNameUrl).toString();

		if (attachmentType.isEmpty() ||
			attachmentUrl.isEmpty())
			continue;

		MessageAttachmentType type = typeByName(attachmentType);

		if (type == VKMessageAttachment::Unknown)
			continue;

		attachments << VKMessageAttachment(type, attachmentUrl, messageId);
	}
}

/*static*/QStringList VKMessageAttachment::toStringList(const VKMessageAttachments& attachments)
{
	QStringList list;

	for(VKMessageAttachment attachment : attachments)
		list << attachment.url();

	return list;
}

/*static*/VKMessageAttachment::MessageAttachmentType VKMessageAttachment::typeByName(const QString& name)
{
	QString lowerName = name.toLower();

	if (lowerName == QString("audio"))
		return VKMessageAttachment::Audio;
	if (lowerName == QString("photo"))
		return VKMessageAttachment::Photo;
	//if (lowerName == QString("video"))
	//	return VKMessageAttachment::Video;
	
	return VKMessageAttachment::Unknown;
}

VKMessageAttachment::MessageAttachmentType VKMessageAttachment::type()
{
	return m_type;
}

QString VKMessageAttachment::url()
{
	return m_url;
}

QVariant VKMessageAttachment::messageId()
{
	return m_messageId;
}