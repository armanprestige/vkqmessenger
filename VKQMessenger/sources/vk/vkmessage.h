#ifndef VKMESSAGE_H
#define VKMESSAGE_H

#include "vkmessageattachment.h"

class VKMessage
{
public:	
    enum MessageType { IncommingMessage, OutcommingMessage };

public:
    VKMessage();
    VKMessage(MessageType type, QVariant messageId, QVariant from, QString text, QVariant timestamp);


	//inline MessageType type() const { return m_type; }

	void setAttachments(const VKMessageAttachments& attachments);

public:
    MessageType m_type;
    QString m_messageId;
    QString m_from;
    QString m_text;
    QString m_timestamp;

    VKMessageAttachments m_audioAttachments;
	VKMessageAttachments m_photoAttachments;
};

Q_DECLARE_METATYPE(VKMessage)

#endif // VKMESSAGE_H
