#include "stdafx.h"
#include "vkmessage.h"
#include "interface/vkmessagewidget.h"

VKMessage::VKMessage()
{
}

VKMessage::VKMessage(MessageType type, QVariant messageId, QVariant from,
                     QString text, QVariant timestamp)
{
    m_type = type;
    m_messageId = messageId.toString();
    m_from = from.toString();
    m_text = text;
    m_timestamp = timestamp.toString();
}

void VKMessage::setAttachments(const VKMessageAttachments& attachments)
{
	for (VKMessageAttachment attachment : attachments)
	{
		if (attachment.type() == VKMessageAttachment::Audio)
		{
			m_audioAttachments << attachment;
		}
		else if(attachment.type() == VKMessageAttachment::Photo)
		{
			m_photoAttachments << attachment;
		}
	}
}