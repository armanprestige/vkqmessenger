#ifndef __STDAFX_H__
#define __STDAFX_H__

#define WRITE_TRACE(__text__) VKQMLogger::getInstance().write(__text__)
#define WRITE_ASSERT_TRACE(__text__) VKQMLogger::getInstance().write(__text__,"vkqm.assert")

#define VKQM_ASSERT(cond,where,what) \
    if ( cond )\
        WRITE_ASSERT_TRACE(where#what);\
    Q_ASSERT_X(cond, where, what)

//# Qt Includes
#include <QAction>
#include <QApplication>
#include <QCloseEvent>
#include <QDate>
#include <QDebug>
#include <QDesktopServices>
#include <QDialog>
#include <QDir>
#include <QEventLoop>
#include <QFile>
#include <QFileDialog>
#include <QFocusEvent>
#include <QHBoxLayout>
#include <QInputDialog>
#include <QKeyEvent>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QMenu>
#include <QMessageBox>
#include <QModelIndex>
#include <QMovie>
#include <QMutex>
#include <QMutexLocker>
#include <QNetworkAccessManager>
#include <QNetworkProxy>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QScrollArea>
#include <QScrollBar>
#include <QSettings>
#include <QShowEvent>
#include <QSortFilterProxyModel>
#include <QSound>
#include <QString>
#include <QSystemTrayIcon>
#include <QTabBar>
#include <QTextStream>
#include <QThread>
#include <QTime>
#include <QTimer>
#include <QUrl>
#include <QUrlQuery>
#include <QVariant>
#include <QVBoxLayout>
#include <QWebView>
#include <QWidget>

//# standard includes
#include <atomic>

// vkqm includes
#include "helpers/qtjson/json.h"
#include "helpers/vkqmlogger.h"
#include "helpers/vkqmthreadworker.h"

#endif // __STDAFX_H__