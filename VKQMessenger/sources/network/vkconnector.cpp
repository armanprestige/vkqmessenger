#include "stdafx.h"
#include "vkconnector.h"
#include "network/requestsender.h"
#include "network/requestbuilder.h"
#include "network/vkavatarsloader.h"
#include "helpers/vkqmutils.h"

#include "vk/vkmessage.h"

/*static*///VKConnector *VKConnector::m_self = nullptr;

VKConnector::VKConnector(QObject *parent)
    : QObject(parent)
{
    qRegisterMetaType<VKMessage>("VKMessage");
}

VKUser VKConnector::parseUser(QVariantMap &node)
{
    QVariant id = node.value("id");
    QString f_name = node.value("first_name").toString();
    QString l_name = node.value("last_name").toString();
    QVariant online = node.value("online");
    QString status = node.value("status").toString();
    bool onlineMobile = node.value("online_mobile").toInt();

    return VKUser(id, f_name, l_name, QPixmap(), online.toBool(), status, onlineMobile);
}

/*static*/VKConnector &VKConnector::getInstance()
{
    // This code is thread-safe becaouse new standard of C++ 11
    // initialize static variables without locking.
    static VKConnector self;
    return self;
}

const VKUser VKConnector::loadSelf()
{
    RequestSender sender;
    Request req = RequestBuilder::currentUserInfo();
    QByteArray answer = VKQMUtils::doubleGet(req, sender);

    QVariantMap rootMap = QtJson::parse(answer).toMap();
    if ( !rootMap.contains("response") )
        throw QString("[VKConnector::loadSelf()] answer don't contain 'response' item.");

    if ( rootMap.value("response").toList().size() != 1)
        throw QString("[VKConnector::loadSelf()] wrong elements count in answer.");

    QVariantMap selfNode = rootMap.value("response").toList().first().toMap();
    m_currentUser = parseUser(selfNode);

    QPixmap pix = VKQMUtils::loadAvatar(selfNode.value("photo_100").toString());
    m_currentUser.setAvatar(pix);

    WRITE_TRACE("SELF LOADED");
    emit selfLoaded();

    return m_currentUser;
}

void VKConnector::updateAvatar(QString id, QPixmap pic)
{
    if ( m_userMap.contains(id) )
    {
        VKUser *user = m_userMap[id];
        user->setAvatar(pic);

        for ( int i = 0; i < m_userList.count(); i++)
        {
            VKUser *luser = m_userList[i];
            if ( luser->id() == id )
                luser->setAvatar(pic);
        }

        emit avatarChanged(id);
    }
}

const VKUserList &VKConnector::loadFriends()
{
    m_userList.clear();

    RequestSender sender;
    QByteArray answer = sender.get(RequestBuilder::friendsList());

    QVariantMap rootMap = QtJson::parse(answer).toMap();
    if ( !rootMap.contains("response") )
        throw QString("[VKConnector::LoadFriends()] answer don't contain 'response' item.");

    QVariantList friendsList = rootMap.value("response").toMap().value("items").toList();
    int count = friendsList.count();

    for( int i = 0; i < count; i++ )
    {
        QVariantMap current = friendsList.at(i).toMap();
        VKUser *user = new VKUser(parseUser(current));
        QString photo_link = current.value("photo_50").toString();

        m_userList.append(user);
        m_userMap.insert(user->id().toString(), user);
        m_avatars.insert(user->id().toString(), photo_link);
    }

    return m_userList;
}
