#ifndef REQUESTSENDER_H
#define REQUESTSENDER_H

#include "request.h"

/*!
 * \brief The RequestSender class
 * Description: RequestSender provide opportinity to do
 * get/post requests and lock thread while waiting for answer
 * if u need asynchronous functions - use QNetworkAccessManager
 * Created by: Arman Oganesyan
 */

class RequestSender : public QObject
{
    Q_OBJECT
public:
    explicit RequestSender(int timeout_ms = 35000, QObject *parent = 0);

    QByteArray get(Request request);
    QByteArray get(QUrl url);
    QByteArray post(Request request);
    QByteArray post(QUrl url, QByteArray data);

private:
    int m_timeout_ms;

    inline QNetworkRequest createRequest(QUrl url)
    {
        return QNetworkRequest(url);
    }
};

#endif // REQUESTSENDER_H
