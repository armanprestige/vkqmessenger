#include "stdafx.h"
#include "requestsender.h"
#include "helpers/vkqmutils.h"
#include "vkavatarsloader.h"

VKAvatarsLoader::VKAvatarsLoader(QMap<QString, QString> objects, QObject *parent)
    : VKQMThreadWorker(parent)
{
    m_objects = objects;
}

VKAvatarsLoader::~VKAvatarsLoader()
{

}

///*static*/QPixmap VKAvatarsLoader::loadAvatar(QString url)
//{
//    if ( !url.contains("/") )
//        return QPixmap();

//    QString fname = url.split("/").last();
//    QString fpath = "avatars/" + fname;

//    QFile file(fpath);
//    if ( file.exists() && file.size() )
//        return QPixmap(fpath);

//    RequestSender sender;
//    QByteArray icon = sender.get(url);

//    if ( file.open(QIODevice::WriteOnly) )
//    {
//        file.write(icon);
//        file.close();
//        return QPixmap(fpath);
//    }

//    return QPixmap();
//}

void VKAvatarsLoader::run()
{
    m_isWorking = true;

    VKQMUtils::mkpath("avatars");

    for(auto itr = m_objects.begin(); itr != m_objects.end() && m_isWorking; itr++)
    {
        QString url = itr.value();

        if ( !url.contains("/") )
        {
            WRITE_TRACE("URL " + url + "DOESN'T CONTAIN URL LIKE TYPE");
            continue;
        }

        QString fname = url.split("/").last();
        QString fpath = "avatars/" + fname;

        QFile file(fpath);
        if ( file.exists() && file.size() )
        {
            emit avatarReady(itr.key(), QPixmap(fpath));
            continue;
        }
        else
            WRITE_TRACE("AVATARS DOESN'T EXIST - HAVE TO DOWNLOAD IT");

        RequestSender sender;
        QByteArray icon = sender.get(url);

        if ( file.open(QIODevice::WriteOnly) )
        {
            file.write(icon);
            file.close();
            emit avatarReady(itr.key (), QPixmap(fpath));
        }
        else
            WRITE_TRACE("ERROR WHILE SAVING AVATAR TO FILE");
    }

    thread()->msleep(1500);

    emit finished();
}
