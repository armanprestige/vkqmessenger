#ifndef VKONLINESETTER_H
#define VKONLINESETTER_H

class VKOnlineSetter : public VKQMThreadWorker
{
    Q_OBJECT
public:
    explicit VKOnlineSetter(QObject *parent = 0);

public slots:
    void run();
};

#endif // VKONLINESETTER_H
