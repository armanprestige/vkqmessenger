#ifndef VKPHOTOLOADER_H
#define VKPHOTOLOADER_H

namespace PhotoSizes {
	enum {
		MinimumSize,
		Photo75 = MinimumSize,
		Photo130,
		Photo604,
		Photo807,
		Photo1280,
		Photo2560,
		MaximumSize = Photo2560
	};
}

extern const char* photoSizes[];

class VKPhotoLoader : public VKQMThreadWorker
{
    Q_OBJECT
public:
    explicit VKPhotoLoader(QString messageId, QString itemUrl, QObject *parent = 0);
    ~VKPhotoLoader();

    //static QPixmap loadAvatar(QString url);

public slots:
    void run() override;

signals:
    /*!
     * \brief photoReady
     * \param QString - path of photo
     */
    void photoReady(QString);

private:
	QString m_messageId;
	QString m_itemUrl;
};

#endif VKPHOTOLOADER_H