#include "stdafx.h"
#include "requestbuilder.h"
#include "helpers/settings.h"

#define API_LANGUAGE    QString("ru")
#define API_VERSION     QString("5.2")
#define API_TRUE        QString("1")
#define API_FALSE       QString("0")

RequestBuilder::RequestBuilder()
{
}

Request RequestBuilder::friendsList()
{
    QUrl url = apiUrl(FriendsGet);
    QUrlQuery query;

    query.addQueryItem("fields", "online,photo_50,id,first_name,last_name");
    query.addQueryItem("name_case", "nom");
    query.addQueryItem("order", Settings::getInstance().value(SettingsValues::SortFriendsPopular).toBool() ? "hints" : "name");

    prependBasic(query);
    url.setQuery(query);

    return Request(url);
}

Request RequestBuilder::currentUserInfo()
{
    QUrl url = apiUrl(UsersGet);
    QUrlQuery query;

    query.addQueryItem("fields", "photo_50,photo_100,status,first_name,last_name");
    query.addQueryItem("order", "name");

    prependBasic(query);
    url.setQuery(query);

    return Request(url);
}

Request RequestBuilder::sendMessage(QVariant to, QVariant text)
{
    QUrl url = apiUrl(MessageSend);
    QUrlQuery query;

    query.addQueryItem("user_id", to.toString());
    query.addQueryItem("message", text.toString());

    prependBasic(query);

    return Request(url, query.toString().toUtf8());
}

Request RequestBuilder::longPollServer(bool useSsl, bool needPts)
{
    QUrl url = apiUrl(LongPollServer);
    QUrlQuery query;

    query.addQueryItem("use_ssl", useSsl ? API_TRUE : API_FALSE);
    query.addQueryItem("need_pts", needPts ? API_TRUE : API_FALSE);

    prependBasic(query);
    url.setQuery(query);

    return Request(url);
}

Request RequestBuilder::changeStatus(QString status)
{
    QUrl url = apiUrl(ChangeStatus);
    QUrlQuery query;

    query.addQueryItem("text", status);

    prependBasic(query);
    url.setQuery(query);

    return Request(url);
}

Request RequestBuilder::markMessageAsRead(QStringList messages)
{
    QUrl url = apiUrl(MarkMessagesAsRead);
    QUrlQuery query;

    query.addQueryItem("message_ids", messages.join(","));

    prependBasic(query);

    return Request(url, query.toString().toUtf8());
}

Request RequestBuilder::setOnline(bool voip)
{
    QUrl url = apiUrl(SetOnline);
    QUrlQuery query;

    query.addQueryItem("voip", voip ? API_TRUE : API_FALSE);

    prependBasic(query);
    url.setQuery(query);

    return Request(url);
}

Request RequestBuilder::audioById(QString identificator)
{
	return RequestBuilder::audioById(QStringList() << identificator);
}

Request RequestBuilder::audioById(QStringList identificators)
{
	QUrl url = apiUrl(AudioById);
	QUrlQuery query;

	query.addQueryItem("audios", identificators.join(','));
	prependBasic(query);
	url.setQuery(query);

	return Request(url);
}

Request RequestBuilder::photoById(QString identificator)
{
	return RequestBuilder::photoById(QStringList() << identificator);
}

Request RequestBuilder::photoById(QStringList identificators)
{
	QUrl url = apiUrl(PhotoById);
	QUrlQuery query;

	query.addQueryItem("photos", identificators.join(','));
	prependBasic(query);
	url.setQuery(query);

	return Request(url);
}

Request RequestBuilder::messageById(QString identificator)
{
	return RequestBuilder::messageById(QStringList() << identificator);
}

Request RequestBuilder::messageById(QStringList identificators, int preview_length /* = 0*/)
{
	QUrl url = apiUrl(MessageById);
	QUrlQuery query;

	query.addQueryItem("message_ids", identificators.join(','));
	query.addQueryItem("preview_length", QString::number(preview_length));
	prependBasic(query);
	url.setQuery(query);

	return Request(url);
}

QUrl RequestBuilder::apiUrl(Method method)
{
    QString url("https://api.vk.com/method/");
    switch(method)
    {
    case FriendsGet: return url.append("friends.get");
    case UsersGet: return url.append("users.get");
    case MessageSend: return url.append("messages.send");
    case LongPollServer: return url.append("messages.getLongPollServer");
    case ChangeStatus: return url.append("status.set");
    case MarkMessagesAsRead: return url.append("messages.markAsRead");
    case SetOnline: return url.append("account.setOnline");
	case AudioById: return url.append("audio.getById");
	case PhotoById: return url.append("photos.getById");
	case MessageById: return url.append("messages.getById");
    case NoMethod: default : return url;
    }
}

void RequestBuilder::prependBasic(QUrlQuery &query, bool addToken)
{
    query.addQueryItem("v", API_VERSION);
    query.addQueryItem("lang", API_LANGUAGE);
    if ( addToken )
        query.addQueryItem("access_token", Settings::getInstance().value(SettingsValues::AccessToken).toString());
}
