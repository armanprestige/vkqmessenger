#ifndef REQUEST_H
#define REQUEST_H

class Request
{
public:
    explicit Request(QString url, QByteArray data = QByteArray());
    explicit Request(QUrl url, QByteArray data = QByteArray());

    inline QUrl url()        { return m_url; }
    inline QByteArray data() { return m_data; }

    enum RequestType { GetRequest, PostRequest };

private:
    QUrl m_url;
    QByteArray m_data;

};

#endif // REQUEST_H
