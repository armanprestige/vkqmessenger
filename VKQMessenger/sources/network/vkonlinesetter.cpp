#include "stdafx.h"
#include "network/requestbuilder.h"
#include "network/requestsender.h"
#include "helpers/settings.h"
#include "vkonlinesetter.h"

VKOnlineSetter::VKOnlineSetter(QObject *parent) :
    VKQMThreadWorker(parent)
{
}

void VKOnlineSetter::run()
{
    m_isWorking = true;

    Request req = RequestBuilder::setOnline();
    RequestSender sender;

    while ( m_isWorking )
    {
        if ( !Settings::getInstance().value(SettingsValues::StayOffline).toBool() )
            sender.get(req);
        thread()->msleep(60000 * 10);
    }

    emit finished();
}
