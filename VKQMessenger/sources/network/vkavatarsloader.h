#ifndef VKAVATARSLOADER_H
#define VKAVATARSLOADER_H

#include "helpers/vkqmthreadworker.h"

class VKAvatarsLoader : public VKQMThreadWorker
{
    Q_OBJECT
public:
    explicit VKAvatarsLoader(QMap<QString,QString> objects, QObject *parent = 0);
    ~VKAvatarsLoader();

    //static QPixmap loadAvatar(QString url);

public slots:
    void run();

signals:
    /*!
     * \brief avatarReady
     * \param QString - id of user
     * \param QPixmap - avatar of user
     */
    void avatarReady(QString, QPixmap);

private:
    QMap<QString,QString> m_objects;
};

#endif // VKAVATARSLOADER_H
