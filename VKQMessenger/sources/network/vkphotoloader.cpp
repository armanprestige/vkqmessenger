#include "stdafx.h"
#include "vkphotoloader.h"
#include "requestsender.h"
#include "requestbuilder.h"
#include "helpers/vkqmutils.h"

const char* photoSizes[] = {
	"photo_75",
	"photo_130",
	"photo_604",
	"photo_807",
	"photo_1280",
	"photo_2560"
};

VKPhotoLoader::VKPhotoLoader(QString messageId, QString itemUrl, QObject *parent)
	: VKQMThreadWorker(parent)
{
	m_messageId = messageId;
	m_itemUrl = itemUrl;
}

VKPhotoLoader::~VKPhotoLoader()
{
}

void VKPhotoLoader::run()
{
	m_isWorking = true;

	VKQMUtils::mkdir("photo");

	Request request = RequestBuilder::messageById(m_messageId);
	RequestSender sender;

	QStringList photoData = m_itemUrl.split('_');

	if (photoData.size() == 2 )
	{
		QString owner_id = photoData.at(0);
		QString photo_id = photoData.at(1);

		QByteArray answer = VKQMUtils::doubleGet(request, sender);

		QVariantMap map = QtJson::parse(answer).toMap();

		if ( map.contains("response") )
		{
			QVariantList messages = map.value("response").toMap().value("items").toList();

			for ( int i = 0; i < messages.size(); i++ )
			{
				QVariantMap message = messages.at(i).toMap();

				if ( message.value("id").toString() != m_messageId )
					continue;

				QVariantList attachmentList = message.value("attachments").toList();

				if ( attachmentList.size() )
				{
					for ( int j = 0; j < attachmentList.count(); j++ )
					{
						QVariantMap attachment = attachmentList.at(j).toMap();

						if ( attachment.value("type").toString() != "photo" )
							continue;

						QVariantMap photoMap = attachment.value("photo").toMap();

						QString photoId = photoMap.value("id").toString();
						QString ownerId = photoMap.value("owner_id").toString();

						if ( photoId != photo_id ||
							 ownerId != owner_id )
							 continue;

						QString photoKey;

						for (int i = PhotoSizes::MaximumSize; i >= PhotoSizes::MinimumSize; i--)
						{
							if ( photoMap.contains(photoSizes[i]) )
							{
								photoKey = photoSizes[i];
								break;
							}
						}
						QString photoUrl = photoMap.value(photoKey).toString();

						QString fpath = "photo/" + m_itemUrl + ".jpg";
						QFile file(fpath);

						QByteArray photo = sender.get(photoUrl);

						if (file.open(QIODevice::WriteOnly))
						{
							file.write(photo);
							file.close();
							emit photoReady(fpath);
						}
						else
							WRITE_TRACE("VKPhotoLoader::run() !file.open(QIODevice::WriteOnly)");

						emit finished();
						return;
					}
				}
			}

		}
	}
}