#ifndef VKLONGPOLLSERVER_H
#define VKLONGPOLLSERVER_H

#include "helpers/vkqmthreadworker.h"
#include "vk/vkmessage.h"


namespace EventTypes {
    enum {
        MessageDeleted      = 0x0,
        MessageFlagsSwap    = 0x1,
        MessageFlagsSet     = 0x2,
        MessageFlagsFlush   = 0x3,
        MessageAdded        = 0x4,
        UserIsOnline        = 0x8,
        UserIsOffline       = 0x9,
        UserBeginInput      = 0x3D
    };
}

namespace MessageFlags {
    enum {
        Unread      = 0x00000001,
        Outbox      = 0x00000002,
        Replied     = 0x00000004,
        Important   = 0x00000008,
        Chat        = 0x00000010,
        Friends     = 0x00000020,
        Spam        = 0x00000040,
        Deleted     = 0x00000080,
        Fixed       = 0x00000100,
        Media       = 0x00000200
    };
}

namespace MessageFlagsFlushParamsIndexes {
    enum {
        MessageID						= 0x1,
        Mask							= 0x2,
        UserID							= 0x3,
        MessageFlagsFlushMinimumParams	= UserID
    };
}

namespace MessageParamsIndexes {
    enum {
        MessageID				= 0x1,
        MessageFlags			= 0x2,
        MessageFromID			= 0x3,
        MessageTimestamp		= 4,
        MessageSubject			= 0x5,
        MessageText				= 0x6,
        MessageMinimumParams	= MessageText,
		MessageAttachments		= 0x7,
		MessageMaximumParams	= MessageAttachments
    };
}

namespace UserOnlineOfflineParamsIndexes {
    enum {
        UserID							= 0x1,
        Extra							= 0x2,
        UserOnlineOfflineMinumumParams	= Extra
    };
}

namespace UserBeginInputParamsIndexes {
	enum {
		UserID						= 0x1,
		Flags						= 0x2,
		UserBeginInputMinimumParams = Flags,
		UserBeginInputMaximumParams = Flags
	};
}

class VKLongPollServer : public VKQMThreadWorker
{
    Q_OBJECT
public:
    explicit VKLongPollServer(QObject *parent = 0);
    ~VKLongPollServer();

public slots:
    void run();

signals:
    void userOnlineChanged(QString,bool,bool);
    void newMessage(VKMessage);
    void messageWasRead(QString, QString);
	void userBeginInput(QString);

private:
    /*!
     * \brief initialize
     * This method used for getting server addres and etc. params
     * for long poll server.
     */
    void initialize();

    QString m_key;
    QString m_server;
    QString m_ts;
    QString m_pts;
};

#endif // VKLONGPOLLSERVER_H
