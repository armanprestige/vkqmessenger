#include "stdafx.h"
#include "requestsender.h"

RequestSender::RequestSender(int timeout_ms, QObject *parent)
   : QObject(parent), m_timeout_ms(timeout_ms)
{
}

QByteArray RequestSender::get(Request request)
{
    return get(request.url());
}

QByteArray RequestSender::get(QUrl url)
{
    WRITE_TRACE("[GET REQUEST]: " + url.toString());
#ifdef QT_DEBUG
    qDebug() << "[GET REQUEST]:" << url.toString();
#endif

    QEventLoop loop;
    auto manager = new QNetworkAccessManager;
    auto reply = manager->get(createRequest(url));
    connect(manager, &QNetworkAccessManager::finished, &loop, &QEventLoop::quit);
    connect(manager, &QNetworkAccessManager::finished, manager, &QNetworkAccessManager::deleteLater);
    QTimer::singleShot(m_timeout_ms, &loop, SLOT(quit()));
    loop.exec();
    QByteArray answer = reply->readAll();
    delete reply;

    WRITE_TRACE("[ANSWER IS]: " + answer);
#ifdef QT_DEBUG
    qDebug() << "[ANSWER IS]:" << answer;
#endif

    return answer;
}

QByteArray RequestSender::post(Request request)
{
    return post(request.url(), request.data());
}

QByteArray RequestSender::post(QUrl url, QByteArray data)
{
    WRITE_TRACE("[POST REQUEST]: " + url.toString());
    WRITE_TRACE("[DATA]: " + data);
#ifdef QT_DEBUG
    qDebug() << "[POST REQUEST]:" << url.toString();
    qDebug() << "[DATA]:" << data;
#endif

    QEventLoop loop;
    auto manager = new QNetworkAccessManager;
    auto reply = manager->post(createRequest(url), data);
    connect(manager, &QNetworkAccessManager::finished, &loop, &QEventLoop::quit);
    connect(manager, &QNetworkAccessManager::finished, manager, &QNetworkAccessManager::deleteLater);
    QTimer::singleShot(m_timeout_ms, &loop, SLOT(quit()));
    loop.exec();
    QByteArray answer = reply->readAll();
    delete reply;

    WRITE_TRACE("[ANSWER IS]: " + answer);
#ifdef QT_DEBUG
    qDebug() << "[ANSWER IS]:" << answer;
#endif

    return answer;
}
