#ifndef REQUESTBUILDER_H
#define REQUESTBUILDER_H

#include "request.h"

class RequestBuilder
{
    RequestBuilder();

    enum Method {
        NoMethod,
        UsersGet,
        FriendsGet,
        MessageSend,
        LongPollServer,
        ChangeStatus,
        MarkMessagesAsRead,
        SetOnline,
		AudioById,
		PhotoById,
		MessageById
    };

public:
    static Request friendsList();
    static Request currentUserInfo();
    static Request sendMessage(QVariant to, QVariant text);
    static Request longPollServer(bool useSsl = true, bool needPts = true);
    static Request changeStatus(QString status);
    static Request markMessageAsRead(QStringList messages);
    static Request setOnline(bool voip = false);
	static Request audioById(QString identificator);
	static Request audioById(QStringList identificators);
	static Request photoById(QString identificator);
	static Request photoById(QStringList identificators);
	static Request messageById(QString identificator);
	static Request messageById(QStringList identificator, int preview_length = 0);

    static QUrl apiUrl(Method method = NoMethod);

private:
    static void prependBasic(QUrlQuery &query, bool addToken = true);

};

#endif // REQUESTBUILDER_H
