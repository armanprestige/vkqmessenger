#include "stdafx.h"
#include "request.h"

Request::Request(QString url, QByteArray data)
{
	m_url = QUrl(url);
	m_data = data;
}

Request::Request(QUrl url, QByteArray data)
{
	m_url = url;
	m_data = data;
}
