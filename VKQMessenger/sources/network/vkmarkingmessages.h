#ifndef VKMARKINGMESSAGES_H
#define VKMARKINGMESSAGES_H

class VKMarkingMessages : public VKQMThreadWorker
{
    Q_OBJECT
public:
    explicit VKMarkingMessages(QStringList messages, QObject *parent = 0);

public slots:
    void run();

private:
    QStringList m_messages;

};

#endif // VKMARKINGMESSAGES_H
