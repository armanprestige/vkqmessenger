#include "stdafx.h"
#include "requestsender.h"
#include "requestbuilder.h"
#include "vklongpollserver.h"
#include "helpers/vkqmutils.h"

VKLongPollServer::VKLongPollServer(QObject *parent)
    : VKQMThreadWorker(parent)
{
}

VKLongPollServer::~VKLongPollServer()
{

}

void VKLongPollServer::run()
{
    m_isWorking = true;

    initialize();

    while ( m_isWorking )
    {
        QString url("https://{$server}?act=a_check&key={$key}&ts={$ts}&wait=25&mode=98");
        url.replace("{$server}", m_server);
        url.replace("{$key}", m_key);
        url.replace("{$ts}", m_ts);

        Request req(url);
        RequestSender sender;

        QByteArray ans = VKQMUtils::doubleGet(req, sender);

        if ( ans.isEmpty() )
        {
            WRITE_TRACE("Long poll returned empty asnwer");
            continue;
        }

        QVariantMap map = QtJson::parse(ans).toMap();
        m_ts = map.value("ts").toString();

        if ( !map.contains("ts") )
            WRITE_TRACE("Coudn't parse 'ts'");

        if ( map.contains("failed") )
        {
            WRITE_TRACE("Have to get server one more time");
            initialize();
            continue;
        }

        QVariantList updates = map.value("updates").toList();
        if ( !updates.size() )
        {
            WRITE_TRACE("No new events");
            continue;
        }

        for ( int i = 0; i < updates.size(); i++ )
        {
            QVariantList cur = updates.at(i).toList();

            if ( cur.size() < 2 )
                continue;

            int type = cur.at(0).toInt();

            if ( type == EventTypes::MessageAdded )
            {
                if ( cur.count() < MessageParamsIndexes::MessageMinimumParams )
                    continue;

                int message_flags = cur.at(MessageParamsIndexes::MessageFlags).toInt();
                QString message_id = cur.at(MessageParamsIndexes::MessageID).toString();
                QString message_from = cur.at(MessageParamsIndexes::MessageFromID).toString();
                QString message_text = cur.at(MessageParamsIndexes::MessageText).toString();
                QString timestamp = cur.at(MessageParamsIndexes::MessageTimestamp).toString();

                int message_type = 0;

                if ( message_flags & MessageFlags::Outbox )
                    message_type = VKMessage::OutcommingMessage;
                else
                    message_type = VKMessage::IncommingMessage;
				
				VKMessage message(VKMessage::MessageType(message_type), message_id,
                                      message_from, message_text,
                                      timestamp);

				if ( cur.count() >= MessageParamsIndexes::MessageAttachments )
				{
					VKMessageAttachments attachments = VKMessageAttachment::parseAttachments(cur.at(MessageParamsIndexes::MessageAttachments).toMap(), message_id);
					message.setAttachments(attachments);
				}

				emit newMessage(message);
            }
            else if ( type == EventTypes::UserIsOnline || type == EventTypes::UserIsOffline )
            {
                if ( cur.count() < UserOnlineOfflineParamsIndexes::UserOnlineOfflineMinumumParams - 1)
                    continue;
                QString id = cur.at(UserOnlineOfflineParamsIndexes::UserID).toString().remove("-");
                bool mobile = false;
                int extra = 0;
                if ( cur.count() >= UserOnlineOfflineParamsIndexes::Extra)
                    extra = cur.at(UserOnlineOfflineParamsIndexes::Extra).toInt();
                mobile = extra > 0 && extra < 6;
                emit userOnlineChanged(id, type == EventTypes::UserIsOnline, mobile);
            }
            else if ( type == EventTypes::MessageFlagsFlush )
            {
                if ( cur.count() < MessageFlagsFlushParamsIndexes::MessageFlagsFlushMinimumParams )
                    continue;

                QString id = cur.at(MessageFlagsFlushParamsIndexes::UserID).toString();
                QString message_id = cur.at(MessageFlagsFlushParamsIndexes::MessageID).toString();
                int message_flags = cur.at(MessageFlagsFlushParamsIndexes::Mask).toInt();

                if ( message_flags & MessageFlags::Unread )
                    emit messageWasRead(id, message_id);
            }
			else if ( type == EventTypes::UserBeginInput )
			{
				if ( cur.count() < UserBeginInputParamsIndexes::UserBeginInputMinimumParams )
					continue;

				QString id = cur.at(UserBeginInputParamsIndexes::UserID).toString();

				emit userBeginInput(id);
			}
        }
    }

    WRITE_TRACE("Stopping long poll server thread.");
    emit finished();
}

void VKLongPollServer::initialize()
{
    Request serverReq = RequestBuilder::longPollServer();
    RequestSender sender;

    QByteArray answer = VKQMUtils::doubleGet(serverReq, sender);

    if ( !answer.contains("response") )
        throw QString("Can't get Long Poll Server!");


    QVariantMap response = QtJson::parse(answer).toMap().value("response").toMap();

    m_key = response.value("key").toString();
    m_server = response.value("server").toString();
    m_ts = response.value("ts").toString();
    m_pts = response.value("pts").toString();

    WRITE_TRACE("Long poll server getted.");
}
