#include "stdafx.h"
#include "network/requestbuilder.h"
#include "network/requestsender.h"
#include "helpers/vkqmutils.h"
#include "vkmarkingmessages.h"

VKMarkingMessages::VKMarkingMessages(QStringList messages, QObject *parent) :
    VKQMThreadWorker(parent)
{
    m_messages = messages;
}

void VKMarkingMessages::run()
{
    m_isWorking = true;

    Request req = RequestBuilder::markMessageAsRead(m_messages);
    RequestSender sender;

    VKQMUtils::doublePost(req, sender);

    emit finished();
}
