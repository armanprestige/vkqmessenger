#ifndef VKCONNECTOR_H
#define VKCONNECTOR_H

#include "vk/vkuser.h"

/*!
 * \brief The VKConnector class
 * Description: print description here
 * Created by: Arman Oganesyan
 */
class VKConnector : public QObject
{
    Q_OBJECT
public:
    static VKConnector& getInstance();  // implemetning singleton

    const VKUserList& loadFriends();
    const VKUser loadSelf();
    inline VKUserNamedMap friendMap() { return m_userMap; }
    inline VKUserList &friendList() { return m_userList; }
    inline VKUser currentUser() { return m_currentUser; }
    inline const QMap<QString,QString>& avatarsSource() { return m_avatars; }

public slots:
    void updateAvatar(QString id, QPixmap pic);

signals:
    void avatarChanged(QString);
    void selfLoaded();

private:
    explicit VKConnector(QObject *parent = 0);      // default ctor

    VKConnector(const VKConnector&);                // copy ctor
    VKConnector& operator= (const VKConnector&);    // operator =
    VKConnector& operator= (const VKConnector&&);   // move semanthic

    VKUser parseUser(QVariantMap &node);

private:
    //static VKConnector *m_self;

    VKUserList m_userList;
    VKUserNamedMap m_userMap;
    VKUser m_currentUser;
    QMap<QString,QString> m_avatars;
};

#endif // VKCONNECTOR_H
