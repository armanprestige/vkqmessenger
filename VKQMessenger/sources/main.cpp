#include "stdafx.h"

#include "helpers/settings.h"
#include "helpers/vkqmutils.h"

#include "interface/vkqmmainwindow.h"
#include "interface/vkqmauthdialog.h"
#include "interface/vkmessagewidget.h"
#include "interface/vktalkwidget.h"
#include "interface/vktalksdialog.h"

#include "network/vkconnector.h"
#include "network/vkavatarsloader.h"
#include "network/vklongpollserver.h"
#include "network/vkonlinesetter.h"

#include <QCoreApplication>
#include <QStyleFactory>

int main(int argc, char *argv[])
{
    WRITE_TRACE("���������� ��������.");
    QApplication a(argc, argv);

    // Set application style
    QApplication::setStyle(QStyleFactory::create("Fusion"));

    // Close application only from main window
    QApplication::setQuitOnLastWindowClosed(false);

    try {
        // Load last session
        Settings::getInstance().load();
		
        // Check for auth - if need - do auth
        if ( !VKQMAuthDialog::isAuthActive() )
        {
            WRITE_TRACE("access_token �� ������� - ����� �����������");
            VKQMAuthDialog adi;

            adi.exec();

            if ( !adi.authSucceed() )
            {
                WRITE_TRACE("����������� �� ��������.");
                return 0;
            }

            WRITE_TRACE("����������� ������� ��������");
        }

        // Load user friends
        VKConnector::getInstance().loadFriends();

        VKQMMainWindow *vkqmmw = new VKQMMainWindow;
        vkqmmw->show();

        QObject::connect(&VKConnector::getInstance(), &VKConnector::avatarChanged,
                         vkqmmw->model(), &FriendsListModel::updateUserAvatar);


        VKAvatarsLoader  *loader = new VKAvatarsLoader(VKConnector::getInstance().avatarsSource());
        VKLongPollServer *server = new VKLongPollServer;

        QObject::connect(server, &VKLongPollServer::newMessage,
                         &VKTalksDialog::getInstance(), &VKTalksDialog::proceedIncomingMessage);
		
        QObject::connect(server, &VKLongPollServer::messageWasRead,
                         &VKTalksDialog::getInstance(), &VKTalksDialog::messageWasRead);
		
		QObject::connect(server, &VKLongPollServer::userBeginInput,
						&VKTalksDialog::getInstance(), &VKTalksDialog::userBeginInput);

        QObject::connect(server, &VKLongPollServer::userOnlineChanged,
                         vkqmmw, &VKQMMainWindow::onUserOnlineChanged);

        QObject::connect(vkqmmw, &VKQMMainWindow::openTalkTabRequest,
                         &VKTalksDialog::getInstance(), &VKTalksDialog::openTalkTab);

        QObject::connect(loader, SIGNAL(avatarReady(QString,QPixmap)),
                         &VKConnector::getInstance(), SLOT(updateAvatar(QString,QPixmap)),
                         Qt::DirectConnection);

        QObject::connect(&VKConnector::getInstance(), SIGNAL(avatarChanged(QString)),
                         &VKTalksDialog::getInstance(), SLOT(userAvatarUpdated(QString)));

        VKQMThreadWorker::startWorkerThread(new VKOnlineSetter);
        VKQMThreadWorker::startWorkerThread(server);
        VKQMThreadWorker::startWorkerThread(loader);
    }
    catch(QString exp) {
        WRITE_ASSERT_TRACE(exp);
    }
    catch(...) {
        WRITE_ASSERT_TRACE("UNKNOWN EXCEPTION TYPE");
    }

    return a.exec();
}
