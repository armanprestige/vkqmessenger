#ifndef VKQMMAINWINDOW_H
#define VKQMMAINWINDOW_H

#include "models/friendslistmodel.h"
#include "models/friendslistmodeldelegate.h"

namespace Ui {
class VKQMMainWindow;
}

class VKQMMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit VKQMMainWindow(QWidget *parent = 0);
    ~VKQMMainWindow();

    const FriendsListModel *model() const { return m_friendsModel; }

signals:
    void openTalkTabRequest(QString);

public slots:
    void onUserOnlineChanged(QString id, bool isOnline, bool fromMobile);

protected:
    void closeEvent(QCloseEvent *e);
    void initStyle();

private slots:
    void checkSoundState();
    void on_buttonPin_clicked();
    void on_buttonChangeStatus_clicked();
    void on_buttonExit_clicked();
    void on_buttonOnlyOnline_clicked();
    void on_buttonSettings_clicked();

    void closeApplication();
    void about();

private:
    void createTrayActions();
    void createTrayIcon();

private:
    Ui::VKQMMainWindow *ui;

    FriendsListModel *m_friendsModel;
    FriendsSortModel *m_friendsProxyModel;
    FriendsListModelDelegate *m_friendsModelDelegate;

    QAction *m_showAction;
    QAction *m_aboutAction;
    QAction *m_closeAction;

    QSystemTrayIcon *m_trayIcon;
    QMenu *m_trayMenu;

    bool m_firstClose;
};

#endif // VKQMMAINWINDOW_H
