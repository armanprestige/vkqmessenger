#ifndef VKTALKSDIALOG_H
#define VKTALKSDIALOG_H

#include <QDialog>
#include <QTabWidget>
#include <QTabBar>

#include "interface/vktalkwidget.h"
#include "interface/vktabwidget.h"

class VKTalksDialog : public QDialog
{
    Q_OBJECT
private:
    explicit VKTalksDialog(QWidget *parent = 0);
    VKTalksDialog(VKTalksDialog&);
    VKTalksDialog& operator=(VKTalksDialog&);
    VKTalksDialog& operator=(VKTalksDialog&&);

public:
    static VKTalksDialog& getInstance();
    QWidget *activeWidget() const;

public slots:
    void openTalkTab(QString id);
    void proceedIncomingMessage(VKMessage message);
    void userAvatarUpdated(QString id);
    void userOnlineChanged(QString id, bool isOnline, bool fromMobile);
    void messageWasRead(QString id, QString message_id);
	void userBeginInput(QString id);

private:
    QMutex m_mutex;
    QMap<QString, VKTalkWidget*> m_talks;

    QVBoxLayout *m_layout;
    VKTabWidget *m_tabWidget;

};


#endif // VKTALKSDIALOG_H
