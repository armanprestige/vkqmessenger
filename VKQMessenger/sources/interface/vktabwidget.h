#ifndef VKTABWIDGET_H
#define VKTABWIDGET_H

class VKTabMovie : public QLabel
{
    Q_OBJECT
public:
    explicit VKTabMovie(QString fileName, QWidget * parent = 0);
    ~VKTabMovie();

signals:
    void started();
    void stopped();

public slots:
    void start();
    void stop();
	void setActive(bool active);

private:
    QMovie *m_movie;
    std::atomic_bool m_isStarted;
};

class VKTabWidget : public QTabWidget
{
    Q_OBJECT
public:
    explicit VKTabWidget(QWidget *parent = 0);

protected:
    void tabInserted(int index);
    void setTabButton(int index, QWidget *widget,
                      QTabBar::ButtonPosition position = QTabBar::LeftSide);

signals:

public slots:
    void startFlashing(int index);
    void startFlashing(QWidget *widget);
    void stopFlashing(QWidget *widget);
    void stopFlashing(int index);
	void setActive(int index);

};

#endif // VKTABWIDGET_H
