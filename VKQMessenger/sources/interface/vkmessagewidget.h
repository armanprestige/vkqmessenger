#ifndef VKMESSAGEWIDGET_H
#define VKMESSAGEWIDGET_H

#include "vk/vkmessage.h"

class VKAudioAttachmentWidget : public QWidget
{
    Q_OBJECT
public:
    explicit VKAudioAttachmentWidget(QWidget *parent = 0);

private:
    void initialize();

    bool m_state;

    QHBoxLayout *m_mainLayout;
    QPushButton *m_playButton;
    QLabel *m_titleLabel;
};

class VKMessageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit VKMessageWidget(VKMessage message, QWidget *parent = 0);

    inline VKMessage message() { return m_message; }

public slots:
    void markMessageAsRead();
    void markMessageAsUnread();

protected:
	void paintEvent(QPaintEvent * event);

private:
    void initialize();

    QLabel *m_messageLabel;
    QVBoxLayout *m_layout;
    VKMessage m_message;

};

#endif // VKMESSAGEWIDGET_H
