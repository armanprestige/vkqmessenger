#ifndef VKQMSETTINGSDIALOG_H
#define VKQMSETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class VKQMSettingsDialog;
}

class VKQMSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VKQMSettingsDialog(QWidget *parent = 0);
    ~VKQMSettingsDialog();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::VKQMSettingsDialog *ui;
};

#endif // VKQMSETTINGSDIALOG_H
