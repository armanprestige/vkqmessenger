#include "stdafx.h"
#include "helpers/settings.h"
#include "network/requestsender.h"
#include "vkqmauthdialog.h"

VKQMAuthDialog::VKQMAuthDialog(QWidget *parent) :
    QDialog(parent)
{
    auto view = new QWebView(this);
    auto layout = new QVBoxLayout(this);
    layout->addWidget(view);
    setLayout(layout);
    setWindowTitle("VK Quick Messenger");

    connect(view, &QWebView::urlChanged, this, &VKQMAuthDialog::checkForToken);

    view->load(QString("https://oauth.vk.com/authorize?client_id=4436034&scope=offline,status,messages,friends,wall,audio,photos&redirect_uri=https://oauth.vk.com/blank.html&display=touch&response_type=token"));
}

void VKQMAuthDialog::checkForToken(QUrl url)
{
    url = url.toString().replace("#", "?");
    QUrlQuery query(url.query());
    if ( !query.queryItemValue("access_token").isEmpty() )
    {
        Settings::getInstance().setValue(SettingsValues::AccessToken, query.queryItemValue("access_token"));
        Settings::getInstance().save();
        m_success = true;
        close();
    }
    else if ( !query.queryItemValue("error_reason").isEmpty() )
        close();
}


bool VKQMAuthDialog::isAuthActive()
{
    RequestSender sender(10000);
    Request request("https://api.vk.com/method/users.get?user_ids=1&v=5.2&access_token=" + Settings::getInstance().value(SettingsValues::AccessToken).toString());
    return sender.get(request).contains("response");
}
