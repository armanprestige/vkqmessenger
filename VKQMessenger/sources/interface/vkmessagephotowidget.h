#ifndef VKMESSAGEPHOTOWIDGET_H
#define VKMESSAGEPHOTOWIDGET_H

#include <QLabel>
#include "ui_vkmessagephotowidget.h"

class VKMessagePhotoWidget : public QLabel
{
	Q_OBJECT

public:
	VKMessagePhotoWidget(QWidget *parent = 0);
	~VKMessagePhotoWidget();

public slots:
	void loadFromFile(QString);
	void open();
	
protected:
	void mouseReleaseEvent(QMouseEvent * e);

private:
	Ui::VKMessagePhotoWidget ui;
	QString m_path;
};

#endif // VKMESSAGEPHOTOWIDGET_H
