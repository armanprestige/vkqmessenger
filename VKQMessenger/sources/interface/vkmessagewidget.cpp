#include "stdafx.h"
#include "vkmessagewidget.h"
#include "vkmessagephotowidget.h"
#include "network/vkconnector.h"
#include "network/vkphotoloader.h"
#include "helpers/vkqmutils.h"

#define PLAY_ICON QIcon("://resources/icons/play-icon.png")
#define PAUSE_ICON QIcon("://resources/icons/pause-icon.png")

#define SENDER_NAME "{$SENDER_NAME}"
#define TIMESTMAP   "{$TIMESTAMP}"
#define MESSAGE_TEXT "{$MESSAGE_TEXT}"
#define SENDER_ID   "{$SENDER_ID}"

//#define INCOMING_MESSAGE_TEMPLATE \ "<font color='#2b587a'><b><a href='http://vk.com/id" SENDER_ID "'>" SENDER_NAME "</a> (" TIMESTMAP "):</b></font><br/>" MESSAGE_TEXT

#define INCOMING_MESSAGE_TEMPLATE \
	"<font color='#2b587a'><b>" SENDER_NAME " (" TIMESTMAP "):</b></font><br/>" MESSAGE_TEXT

#define OUTCOMING_MESSAGE_TEMLATE \
	"<font color='#4682B4'><b>" SENDER_NAME " (" TIMESTMAP "):</b></font><br/>" MESSAGE_TEXT


VKAudioAttachmentWidget::VKAudioAttachmentWidget(QWidget *parent)
	: QWidget(parent)
{
	initialize();
}

void VKAudioAttachmentWidget::initialize()
{
	m_state = false;

	m_playButton = new QPushButton();
	m_playButton->setFixedSize(22, 22);
	m_playButton->setIcon(PLAY_ICON);
	m_playButton->setStyleSheet("border: 0px;");

	m_titleLabel = new QLabel("<span style=\"color: #2b587a\"><b>Stromae</b></span> - Papaoutai [3:52]");

	m_mainLayout = new QHBoxLayout;
	m_mainLayout->addWidget(m_playButton);
	m_mainLayout->addWidget(m_titleLabel);

	connect(m_playButton, &QPushButton::clicked, this, [=]()
	{
		if (m_state)
			m_playButton->setIcon(PLAY_ICON);
		else
			m_playButton->setIcon(PAUSE_ICON);
		m_state = !m_state;
	});

	setLayout(m_mainLayout);
}

VKMessageWidget::VKMessageWidget(VKMessage message, QWidget *parent) :
	QWidget(parent)
{
	m_message = message;
	initialize();
}

void VKMessageWidget::markMessageAsRead()
{
	setStyleSheet("");
}

void VKMessageWidget::markMessageAsUnread()
{
	setStyleSheet("background-color: #D1DAE3;");
}

void VKMessageWidget::paintEvent(QPaintEvent * event)
{
	//QPainter painter(this);
	//painter.setPen(Qt::black);
	//painter.drawRoundRect(event->rect());

	QWidget::paintEvent(event);
}

void VKMessageWidget::initialize()
{
	QString sender_name;
	QString message;

	if ( m_message.m_type == VKMessage::OutcommingMessage )
	{
		sender_name = VKConnector::getInstance().currentUser().name();
		message = OUTCOMING_MESSAGE_TEMLATE;
	}
	else
	{
		sender_name = VKConnector::getInstance().friendMap()[m_message.m_from]->name();
		message = INCOMING_MESSAGE_TEMPLATE;
	}

	//message.replace(SENDER_ID, m_message.m_from);
	message.replace(SENDER_NAME, sender_name);
	message.replace(TIMESTMAP, VKQMUtils::fromUnixTimestamp(m_message.m_timestamp.toUInt()));
	message.replace(MESSAGE_TEXT, m_message.m_text);

	m_messageLabel = new QLabel;
	m_messageLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
	m_messageLabel->setWordWrap(true);
	m_messageLabel->setScaledContents(true);
	m_messageLabel->setText(message);

	//if ( m_message.m_type == VKMessage::IncommingMessage )
	//	m_messageLabel->setAlignment(Qt::AlignRight);
	//else
	//	m_messageLabel->setAlignment(Qt::AlignLeft);




	m_layout = new QVBoxLayout;
	m_layout->addWidget(m_messageLabel);

	for ( VKMessageAttachment attachment : m_message.m_photoAttachments )
	{
		if ( attachment.type() == VKMessageAttachment::Photo )
		{
			VKMessagePhotoWidget* widget = new VKMessagePhotoWidget(this);
			m_layout->addWidget(widget);
			//if ( m_message.m_type == VKMessage::IncommingMessage )
			//	m_layout->addWidget(widget, 0, Qt::AlignRight);
			//else
			//	m_layout->addWidget(widget, 0, Qt::AlignLeft);


			VKPhotoLoader* loader = new VKPhotoLoader(attachment.messageId().toString(), attachment.url());
			connect(loader, &VKPhotoLoader::photoReady, widget, &VKMessagePhotoWidget::loadFromFile);
			VKPhotoLoader::startWorkerThread(loader);
		}
	}

	setLayout(m_layout);

	setAttribute(Qt::WA_StyledBackground, true);
	markMessageAsUnread();

	setMinimumWidth(300);

	adjustSize();
}
