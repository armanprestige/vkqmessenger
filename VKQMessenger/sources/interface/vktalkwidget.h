#ifndef VKTALKWIDGET_H
#define VKTALKWIDGET_H

#include "vk/vkmessage.h"
#include "vk/vkuser.h"

#include <QEvent>
#include <QWidget>

class VKMessageWidget;
class VKQMInputMessageEdit;
class VKUserBeginInput;

/*!
 * \brief The VKTalkWidget class
 * Description:
 * Created by: Arman Oganesyan
 */
class VKTalkWidget : public QWidget
{
    Q_OBJECT
public:
    explicit VKTalkWidget(VKUser *user, QWidget *parent = 0);
    ~VKTalkWidget();

    void setBuddyAvatar(QPixmap &pix);
    VKUser *buddy();

public slots:
    void scrollToBottom();
    void addMessage(VKMessageWidget* msgWidget);
    void addMessage(VKMessage msg);

    void setUserOnline(bool isOnline, bool fromMobile);
    void sendMessage();
    void messageWasRead(QString id);
    void markInMessagesAsRead();
	void userBeginInput();

protected:
    bool eventFilter(QObject *o, QEvent *e);
	void resizeEvent(QResizeEvent * event);

private:
    void initialize();

    VKUser *m_user;
    QMap<QString, VKMessageWidget*> m_messages;
    QMap<QString, VKMessageWidget*> m_unreadMessages;

    // main controls
    QVBoxLayout *m_mainLayout;
    QLabel *m_avatar;

    // for messages
    QScrollArea *m_scrollArea;
    QWidget *m_scrollAreaContentWidget;
    QVBoxLayout *m_messagesLayout;
    VKQMInputMessageEdit *m_inputMessageEdit;
    QPushButton *m_sendMessageButton;
	VKUserBeginInput *m_userBeginInput;
    QLabel *m_onlineLabel;
};

/*!
 * \brief The VKQMInputMessageEdit class
 * Description:
 * Created by: Arman Oganesyan
 */
class VKQMInputMessageEdit : public QPlainTextEdit
{
    Q_OBJECT
public:
    VKQMInputMessageEdit(QWidget *parent = 0);

protected:
    void keyPressEvent(QKeyEvent *e);

signals:
    void readySendMessage();
};

/*!
 * \brief The VKUserBeginInput class
 * Description:
 * Created by: Arman Oganesyan
 */
class VKUserBeginInput : public QWidget
{
	Q_OBJECT
public:
	explicit VKUserBeginInput(QString userName, QWidget* parent = 0);

public slots:
	void setUserBeginInput();
	void setUserEndInput();

signals:
	void userStartInput();
	void userEndInput();

private:
	QString m_text;
	QLabel* m_textLabel;
	QLabel* m_movieLabel;
	QMovie* m_movie;
	QTimer* m_timer;
	QMutex m_mutex;
};

#endif // VKTALKWIDGET_H
