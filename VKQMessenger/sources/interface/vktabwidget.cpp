#include "stdafx.h"
#include "vktabwidget.h"
#include "defines.h"

VKTabMovie::VKTabMovie(QString fileName, QWidget *parent)
	: QLabel(parent)
{
	m_movie = new QMovie(fileName);
	m_movie->setScaledSize(QSize(16,16));
	m_movie->setParent(this);
	setMovie(m_movie);
	setFixedSize(16, 16);
	m_movie->stop();
	m_isStarted = false;
	setActive(true);
}

VKTabMovie::~VKTabMovie()
{

}

void VKTabMovie::start()
{
	if ( !m_isStarted )
	{
		m_isStarted = true;
		setMovie(m_movie);
		m_movie->start();
		emit started();
	}
}

void VKTabMovie::stop()
{
	if ( m_isStarted )
	{
		m_isStarted = false;
		m_movie->stop();
		clear();
		emit stopped();
	}
}

void VKTabMovie::setActive(bool active)
{
	if (active)
		setStyleSheet("background-color: #D1DAE3;");
	else
		setStyleSheet("");
}

//////////////////////////////////////////////////////////////////

VKTabWidget::VKTabWidget(QWidget *parent) :
	QTabWidget(parent)
{
	connect(this, &QTabWidget::currentChanged, this, [=](int index)
	{
		stopFlashing(index);
		setActive(index);
	});
}

void VKTabWidget::tabInserted(int index)
{
	QWidget * button = tabBar()->tabButton(index, QTabBar::LeftSide);
	if ( button ) return;

	VKTabMovie *_button = new VKTabMovie(":/resources/icons/message2_16x16.gif", widget(index));
	tabBar()->setTabButton(index, QTabBar::LeftSide, _button);
	//_button->start();
}

void VKTabWidget::startFlashing(int index)
{
	if ( index != -1 )
	{
		VKTabMovie *movie = dynamic_cast<VKTabMovie*>(tabBar()->tabButton(index, QTabBar::LeftSide));
		if (movie != nullptr)
			movie->start();
	}
}

void VKTabWidget::startFlashing(QWidget *widget)
{
	startFlashing(indexOf(widget));
}

void VKTabWidget::stopFlashing(int index)
{
	if ( index != -1 )
	{
		VKTabMovie *movie = dynamic_cast<VKTabMovie*>(tabBar()->tabButton(index, QTabBar::LeftSide));
		if (movie != nullptr)
			movie->stop();
	}
}

void VKTabWidget::stopFlashing(QWidget *widget)
{
	stopFlashing(indexOf(widget));
}

void VKTabWidget::setActive(int index)
{
	//if ( index != -1 )
	//{
		for (int i = 0; i < count(); ++i)
		{
			VKTabMovie *movie = dynamic_cast<VKTabMovie*>(tabBar()->tabButton(i, QTabBar::LeftSide));
			if (movie != nullptr)
				movie->setActive(false);
		}

		VKTabMovie *movie = dynamic_cast<VKTabMovie*>(tabBar()->tabButton(index, QTabBar::LeftSide));
		if (movie != nullptr)
			movie->setActive(true);		
	//}
}