#include "stdafx.h"
#include "vkmessagephotowidget.h"

VKMessagePhotoWidget::VKMessagePhotoWidget(QWidget *parent)
	: QLabel(parent)
{
	ui.setupUi(this);
	clear();
	setFixedSize(1, 150);
}

VKMessagePhotoWidget::~VKMessagePhotoWidget()
{

}

void VKMessagePhotoWidget::mouseReleaseEvent(QMouseEvent * e)
{
	if ( e->button() == Qt::LeftButton )
		open();
	QLabel::mouseReleaseEvent(e);
}

void VKMessagePhotoWidget::loadFromFile(QString path)
{
	m_path = path;

	QPixmap pixmap(m_path);

	int maxW = 300;
	int maxH = 300;

	const int picW = pixmap.width();
	const int picH = pixmap.height();

	if ( maxW > picW ) maxW = picW;
	if ( maxH > picH ) maxH = picH;

	if ( picW >= picH )
	{
		float ratio = (float) picH / picW;
		setFixedWidth(maxW);
		setFixedHeight(maxW * ratio);
	}else{
		float ratio = (float) picW / picH;
		setFixedHeight(maxH);
		setFixedWidth(maxH * ratio);
	}

	setScaledContents(true);
	setPixmap(pixmap);
}

void VKMessagePhotoWidget::open()
{
	if ( !m_path.isEmpty() )
		QDesktopServices::openUrl(QUrl::fromLocalFile(m_path));
}