#include "stdafx.h"
#include "helpers/settings.h"
#include "vkqmsettingsdialog.h"
#include "ui_vkqmsettingsdialog.h"

VKQMSettingsDialog::VKQMSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VKQMSettingsDialog)
{
    ui->setupUi(this);
    setWindowFlags(windowFlags() ^ Qt::WindowContextHelpButtonHint);

    QPushButton * ok = ui->buttonBox->button(QDialogButtonBox::Ok);
    QPushButton * cancel = ui->buttonBox->button(QDialogButtonBox::Cancel);

    if ( ok && cancel )
    {
        ok->setText(tr("Сохранить"));
        cancel->setText(tr("Отмена"));
    }

    ui->checkBoxSortFriendsPopular->setChecked(Settings::getInstance().value(SettingsValues::SortFriendsPopular).toBool());
    ui->checkBoxStayOffline->setChecked(Settings::getInstance().value(SettingsValues::StayOffline).toBool());
    ui->lineEditProxyAddress->setText(Settings::getInstance().value(SettingsValues::ProxyAddress).toString());
    ui->lineEditProxyLogin->setText(Settings::getInstance().value(SettingsValues::ProxyLogin).toString());
    ui->lineEditProxyPassword->setText(Settings::getInstance().value(SettingsValues::ProxyPassword).toString());
    ui->lineEditProxyPort->setText(Settings::getInstance().value(SettingsValues::ProxyPort).toString());
}

VKQMSettingsDialog::~VKQMSettingsDialog()
{
    delete ui;
}

void VKQMSettingsDialog::on_buttonBox_accepted()
{
    Settings::getInstance().setValue(SettingsValues::SortFriendsPopular, ui->checkBoxSortFriendsPopular->isChecked());
    Settings::getInstance().setValue(SettingsValues::StayOffline, ui->checkBoxStayOffline->isChecked());
    Settings::getInstance().setValue(SettingsValues::ProxyAddress, ui->lineEditProxyAddress->text());
    Settings::getInstance().setValue(SettingsValues::ProxyLogin, ui->lineEditProxyLogin->text());
    Settings::getInstance().setValue(SettingsValues::ProxyPassword, ui->lineEditProxyPassword->text());
    Settings::getInstance().setValue(SettingsValues::ProxyPort, ui->lineEditProxyPort->text());
    Settings::getInstance().save();

    QNetworkProxy::setApplicationProxy(
                Settings::getInstance().value(SettingsValues::ProxyAddress).toString().isEmpty() ?
                QNetworkProxy()
                  :
                QNetworkProxy(QNetworkProxy::HttpProxy,
                              Settings::getInstance().value(SettingsValues::ProxyAddress).toString(),
                              Settings::getInstance().value(SettingsValues::ProxyPort).toInt(),
                              Settings::getInstance().value(SettingsValues::ProxyLogin).toString(),
                              Settings::getInstance().value(SettingsValues::ProxyPassword).toString()
                              )
                );
    close();
}

void VKQMSettingsDialog::on_buttonBox_rejected()
{
    close();
}
