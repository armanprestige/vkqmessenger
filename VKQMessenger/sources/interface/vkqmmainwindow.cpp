#include "stdafx.h"

#include "network/vkconnector.h"
#include "network/requestbuilder.h"
#include "network/requestsender.h"

#include "helpers/settings.h"

#include "interface/vktalksdialog.h"
#include "interface/vkqmsettingsdialog.h"

#include "vkqmmainwindow.h"
#include "ui_vkqmmainwindow.h"

VKQMMainWindow::VKQMMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VKQMMainWindow)
{
    ui->setupUi(this);
    setObjectName("vkqmMainWindow");

    initStyle();

    connect(ui->buttonSound, &QPushButton::toggled, this, &VKQMMainWindow::checkSoundState);
    connect(ui->buttonSound, &QPushButton::clicked, this, &VKQMMainWindow::checkSoundState);

    m_firstClose = true;

    ui->buttonSound->setChecked(!Settings::getInstance().value(SettingsValues::IncommingSound).toBool());

    createTrayActions();
    createTrayIcon();

    m_trayIcon->show();

    VKConnector::getInstance().loadSelf();

    ui->selfAvatarLabel->setPixmap(VKConnector::getInstance().currentUser().avatar());
    ui->selfNameLabel->setText(VKConnector::getInstance().currentUser().name());
    ui->selfStatusLabel->setText(VKConnector::getInstance().currentUser().status());

    m_friendsModel = new FriendsListModel;
    m_friendsModelDelegate = new FriendsListModelDelegate;
    m_friendsProxyModel = new FriendsSortModel;

    m_friendsProxyModel->setSourceModel(m_friendsModel);

    ui->listView->setModel(m_friendsProxyModel);
    ui->listView->setItemDelegate(m_friendsModelDelegate);

    ui->listView->setIconSize(QSize(50, 50));
    ui->listView->setViewMode(QListView::IconMode);

    connect(ui->lineSearchFriend, &QLineEdit::textChanged, this, [&](QString key)
    {
        m_friendsProxyModel->setSearchString(key.toLower());
        m_friendsProxyModel->invalidate();
    });

    connect(ui->listView, &QListView::doubleClicked, this, [=](QModelIndex index)
    {
        QVariant id = m_friendsModel->data(m_friendsProxyModel->mapToSource(index), VKUserIdRole);
        emit openTalkTabRequest(id.toString());
    });

    m_friendsProxyModel->showOnlyOnline(false);
}

VKQMMainWindow::~VKQMMainWindow()
{
    delete ui;
}

void VKQMMainWindow::onUserOnlineChanged(QString id, bool isOnline, bool fromMobile)
{
    if ( VKConnector::getInstance().friendMap().contains(id) )
    {
        VKConnector::getInstance().friendMap()[id]->setIsOnline(isOnline);
        VKTalksDialog::getInstance().userOnlineChanged(id, isOnline, fromMobile);
        m_friendsModel->updateUserOnline(id);
    }
}

void VKQMMainWindow::closeEvent(QCloseEvent *e)
{
    hide();
    e->ignore();

    if ( m_firstClose )
    {
        m_firstClose = false;
        m_trayIcon->showMessage("Кстати...", "Vk.QM продолжает работать в фоновом режиме. Чтобы закрыть его, нажмите правой кнопкой по иконке и выберите соответствующий пункт.");
    }
}

void VKQMMainWindow::initStyle()
{
    //QFile f("://resources/styles/style.css");
    //f.open(QIODevice::ReadOnly);
    //qApp->setStyleSheet(f.readAll());
}

void VKQMMainWindow::checkSoundState()
{
    Settings::getInstance().setValue(SettingsValues::IncommingSound, !ui->buttonSound->isChecked());
}

void VKQMMainWindow::on_buttonPin_clicked()
{
    if ( ui->buttonPin->isChecked() )
        setWindowFlags( (windowFlags() ^ Qt::WindowStaysOnBottomHint ) | Qt::WindowStaysOnTopHint);
    else
        setWindowFlags( (windowFlags() ^ Qt::WindowStaysOnTopHint ) | Qt::WindowStaysOnBottomHint);
    show();
}

void VKQMMainWindow::on_buttonChangeStatus_clicked()
{
    QString status = QInputDialog::getText(this, tr("Изменение статуса"),
                                           tr("Введите новый статус:"),
                                           QLineEdit::Normal, tr("Vk.QM | Мессенджер для ВКонтакте http://vk.com/vk_qm")
										   ,nullptr, Qt::WindowCloseButtonHint);

    if ( status.isEmpty() )
        return;

    Request req = RequestBuilder::changeStatus(status);
    RequestSender sender;
    QByteArray answer = sender.get(req);

    if ( answer.contains("response") )
        ui->selfStatusLabel->setText(status);
}

void VKQMMainWindow::on_buttonExit_clicked()
{
    closeApplication();
}

void VKQMMainWindow::on_buttonOnlyOnline_clicked()
{
    m_friendsProxyModel->showOnlyOnline(ui->buttonOnlyOnline->isChecked());
    m_friendsProxyModel->invalidate();
}

void VKQMMainWindow::on_buttonSettings_clicked()
{
    VKQMSettingsDialog di(this);
    di.exec();
}

void VKQMMainWindow::closeApplication()
{
	int q = QMessageBox::question(isVisible() ? this : nullptr, tr("Подтвердите действие"),
                          tr("Все активные диалоги будут закрыты. Вы действительно хотите закрыть Vk.QM?"),
                          QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

    if ( q == QMessageBox::Yes )
    {
        m_trayIcon->setVisible(false);
        QApplication::quit();
    }
}

void VKQMMainWindow::about()
{
    QMessageBox::about(this,
                       "О программе...",
                       "<center><b>Vk.QM</b><br/>"
                "Бета-версия от 5 апреля 2015 года.<br/>"
                "<b>Разработчик:</b> Арман Оганесян<br/>"
                "Skype: arman.oganesyan");
}

void VKQMMainWindow::createTrayActions()
{
    m_showAction = new QAction("Показать Vk.QM", this);
    connect(m_showAction, &QAction::triggered, this, &VKQMMainWindow::showNormal);

    m_aboutAction = new QAction("О программе...", this);
    connect(m_aboutAction, &QAction::triggered, this, &VKQMMainWindow::about);

    m_closeAction = new QAction("Закрыть Vk.QM", this);
    connect(m_closeAction, &QAction::triggered, this, &VKQMMainWindow::closeApplication);
}

void VKQMMainWindow::createTrayIcon()
{
    m_trayMenu = new QMenu();
    m_trayMenu->addAction(m_showAction);
    m_trayMenu->addSeparator();
    m_trayMenu->addAction(m_aboutAction);
    m_trayMenu->addAction(m_closeAction);

    m_trayIcon = new QSystemTrayIcon(this);
    m_trayIcon->setContextMenu(m_trayMenu);
    m_trayIcon->setIcon(QIcon("://icon.ico"));

    m_trayIcon->setToolTip("Vk.QM | http://vk.com/arman.oganesyan");

    connect(m_trayIcon, &QSystemTrayIcon::activated, this, [=](QSystemTrayIcon::ActivationReason reason)
    {
       if(reason == QSystemTrayIcon::DoubleClick)
           show();
    });
}
