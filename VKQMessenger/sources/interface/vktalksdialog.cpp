#include "stdafx.h"
#include "vktalksdialog.h"
#include "network/vkconnector.h"

VKTalksDialog::VKTalksDialog(QWidget *parent) :
    QDialog(parent)
{
    m_talks.clear();

    m_tabWidget = new VKTabWidget();
    m_tabWidget->setTabsClosable(true);
    m_tabWidget->setMovable(true);
    m_tabWidget->setStyleSheet(
                "QTabBar::tab {"
                "border: 1px solid #C4C4C3;"
                "border-bottom-color: #C2C7CB;"
                "border-top-left-radius: 4px;"
                "border-top-right-radius: 4px;"
                "padding: 1px 3px;"
                "margin-left: 1px;"
                "margin-bottom: 2px;"
                "}"
                "QTabBar::tab:selected {"
                "background-color: #D1DAE3;"
                "}");

    m_layout = new QVBoxLayout;
    m_layout->addWidget(m_tabWidget);

    setLayout(m_layout);
    setStyleSheet("background-color: white");

    setWindowFlags( (windowFlags() | Qt::WindowMinMaxButtonsHint) ^ Qt::WindowContextHelpButtonHint);
    setMinimumHeight(480);
    setMinimumWidth(640);

    connect(m_tabWidget, &VKTabWidget::currentChanged, this, [=](int index)
    {
        QMutexLocker locker(&m_mutex);

        if ( index == -1)
            close();

        auto *current = dynamic_cast<VKTalkWidget*>(m_tabWidget->widget(index));

        VKQM_ASSERT( current != 0, "QTabWidget::currentChanged ", "ptr current is nullptr");

        if ( current )
            current->markInMessagesAsRead();
    });

    connect(m_tabWidget, &QTabWidget::tabCloseRequested, this, [=](int index)
    {
        QMutexLocker locker(&m_mutex);

        if ( index == -1)
            return;

        auto *current = dynamic_cast<VKTalkWidget*>(m_tabWidget->widget(index));

        VKQM_ASSERT( current != 0, "QTabWidget::tabCloseRequested ", "ptr current is nullptr");

        if ( !current )
            return;

        locker.unlock();
        m_tabWidget->removeTab(index);
        locker.relock();

        m_talks.remove(current->buddy()->id().toString());
        current->deleteLater();
    });
}

VKTalksDialog &VKTalksDialog::getInstance()
{
    // This code is thread-safe due to new standard of C++ 11
    // initialize static variables without locking.
    static VKTalksDialog self;
    return self;
}

QWidget *VKTalksDialog::activeWidget() const
{
    return m_tabWidget->currentWidget();
}

void VKTalksDialog::openTalkTab(QString id)
{
    QMutexLocker locker(&m_mutex);
    if ( !m_talks.contains(id) )
    {
        if ( !VKConnector::getInstance().friendMap().contains(id) )
            return;

        VKUser *user = VKConnector::getInstance().friendMap().value(id);
        VKTalkWidget *widget = new VKTalkWidget(user);
        m_talks.insert(id, widget);
        locker.unlock();
        m_tabWidget->addTab(widget, user->name());
    }

    locker.relock();
    int index = m_tabWidget->indexOf(m_talks.value(id));
    locker.unlock();
    m_tabWidget->setCurrentIndex(index);
    show();
}

void VKTalksDialog::proceedIncomingMessage(VKMessage message)
{
    QMutexLocker locker(&m_mutex);
    if ( !m_talks.contains(message.m_from) )
    {
        // don't proceed messages from unknown
        if ( !VKConnector::getInstance().friendMap().contains(message.m_from) )
            return;

        VKUser *user = VKConnector::getInstance().friendMap().value(message.m_from);
        VKTalkWidget *widget = new VKTalkWidget(user, this);
        m_talks.insert(message.m_from, widget);

        locker.unlock();
        m_tabWidget->addTab(widget, user->name());
		m_tabWidget->setActive(m_tabWidget->currentIndex());
    }

    locker.relock();

    m_talks.value(message.m_from)->addMessage(message);
    m_talks.value(message.m_from)->show();

    if ( !this->isVisible() )
        this->showMinimized();

    if ( !isActiveWindow() && message.m_type == VKMessage::IncommingMessage )
    {
        if ( m_tabWidget->currentWidget() != m_talks.value(message.m_from) )
            m_tabWidget->startFlashing(m_talks.value(message.m_from));
        QApplication::alert(this);
    }
}

void VKTalksDialog::userAvatarUpdated(QString id)
{
    QMutexLocker locker(&m_mutex);
    if ( m_talks.contains(id) )
        m_talks[id]->setBuddyAvatar(VKConnector::getInstance().friendMap()[id]->avatar());
}

void VKTalksDialog::userOnlineChanged(QString id, bool isOnline, bool fromMobile)
{
    QMutexLocker locker(&m_mutex);
    if ( m_talks.contains(id) )
        m_talks[id]->setUserOnline(isOnline, fromMobile);
}

void VKTalksDialog::messageWasRead(QString id, QString message_id)
{
    QMutexLocker locker(&m_mutex);
    if ( m_talks.contains(id) )
        m_talks[id]->messageWasRead(message_id);
}

void VKTalksDialog::userBeginInput(QString id)
{
    QMutexLocker locker(&m_mutex);
    if ( m_talks.contains(id) )
		m_talks[id]->userBeginInput();
}