#include "stdafx.h"
#include "vkmessagewidget.h"
#include "network/requestsender.h"
#include "network/requestbuilder.h"
#include "network/vkmarkingmessages.h"
#include "helpers/settings.h"
#include "helpers/vkqmutils.h"
#include "vktalksdialog.h"
#include "vktalkwidget.h"


VKTalkWidget::VKTalkWidget(VKUser *user, QWidget *parent) :
    QWidget(parent)
{
    m_user = user;
    installEventFilter(this);
    initialize();
}

VKTalkWidget::~VKTalkWidget()
{
    for ( auto itr = m_messages.begin(); itr != m_messages.end(); itr++ )
        delete itr.value();
    m_messages.clear();
}

void VKTalkWidget::setBuddyAvatar(QPixmap &pix)
{
    m_avatar->setPixmap(pix);
    m_user->setAvatar(pix);
}

VKUser *VKTalkWidget::buddy()
{
    return m_user;
}

void VKTalkWidget::addMessage(VKMessageWidget *msgWidget)
{
    if ( msgWidget->message().m_type == VKMessage::IncommingMessage )
    {
		if ( Settings::getInstance().value(SettingsValues::IncommingSound).toBool() )
		{
			VKQMUtils::playSound
				(
#ifdef QT_DEBUG
				"sounds\\incomming.mp3"
#else
				"sounds\\incomming.mp3"
#endif
				);
		}

        m_unreadMessages.insert(msgWidget->message().m_messageId, msgWidget);

        if ( isVisible() &&
             isActiveWindow() &&
             !isMinimized() &&
             VKTalksDialog::getInstance().activeWidget() == this
             )
            markInMessagesAsRead();
    }

    m_messages.insert(msgWidget->message().m_messageId, msgWidget);
	//if (msgWidget->message().m_type == VKMessage::IncommingMessage)
	//	m_messagesLayout->addWidget(msgWidget, 0, Qt::AlignRight);
	//else
		m_messagesLayout->addWidget(msgWidget);

    QTimer::singleShot(100, this, SLOT(scrollToBottom()));

	if (!msgWidget->message().m_audioAttachments.isEmpty())
	{
		RequestSender sender;

		QByteArray answer = sender.get(RequestBuilder::audioById(msgWidget->message().m_audioAttachments.first().url()));
		if ( answer.isEmpty() )
			return;

		QVariantMap map = QtJson::parse(answer).toMap();
		QString url = map.value("response").toList().first().toMap().value("url").toString();
	
		QMediaPlayer* pl = new QMediaPlayer();
		pl->setMedia(QUrl(url));
		pl->setVolume(80);
		pl->play();

	}

}

void VKTalkWidget::addMessage(VKMessage msg)
{
    addMessage(new VKMessageWidget(msg));
}

void VKTalkWidget::setUserOnline(bool isOnline, bool fromMobile)
{
    if ( isOnline )
        m_onlineLabel->setText(fromMobile ? QString("<img src=\"%0\"/> online").arg("://resources/icons/mobile.png") :
                                            "online");
    else
        m_onlineLabel->clear();
}

void VKTalkWidget::sendMessage()
{
	QString text = m_inputMessageEdit->toPlainText();
    m_inputMessageEdit->clear();

	if (text.isEmpty())
		return;

    m_sendMessageButton->setDisabled(true);

    RequestSender sender;
    Request req = RequestBuilder::sendMessage(m_user->id(), text);

    forever
    {
        QByteArray ans = sender.post(req);
        if ( ans.contains("response") )
            break;

        int r = QMessageBox::question(this, tr("Ошибка при отправке сообщения"),
                              tr("Сообщение не удалось отправить. Возможно, необходимо ещё раз пройти авторизацию или проблемы с интернетом/сервером. Но мы можем попробовать отправить еще раз. Попробовать?"),
                              QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);

        if ( r == QMessageBox::No )
		{
			m_inputMessageEdit->setPlainText(text + "\n" + m_inputMessageEdit->toPlainText());
            break;
		}
    }

    m_sendMessageButton->setEnabled(true);
}

void VKTalkWidget::messageWasRead(QString id)
{
    if ( m_messages.contains(id) )
        m_messages[id]->markMessageAsRead();
}

void VKTalkWidget::markInMessagesAsRead()
{
    if ( m_unreadMessages.isEmpty() )
        return;
    VKMarkingMessages *marking = new VKMarkingMessages(m_unreadMessages.keys());
    VKMarkingMessages::startWorkerThread(marking);

    m_unreadMessages.clear();
}

void VKTalkWidget::userBeginInput()
{
	this->m_userBeginInput->setUserBeginInput();
}

bool VKTalkWidget::eventFilter(QObject *o, QEvent *e)
{
    QEvent::Type t = e->type();

    if ( t == QEvent::WindowActivate || t == QEvent::Show || t == QEvent::FocusIn)
        if ( isVisible() && isActiveWindow() && !isMinimized() && VKTalksDialog::getInstance().activeWidget() == this )
            markInMessagesAsRead();

    return QWidget::eventFilter(o, e);
}

void VKTalkWidget::resizeEvent(QResizeEvent * event)
{

	//int size = (float)event->size().width() * 0.35;
	//auto beg = m_messages.begin();
	//auto end = m_messages.end();

	//for ( ; beg != end; beg++ )
	//{
	//	VKMessageWidget* w = beg.value();
	//	QMargins margin = w->contentsMargins();

	//	if ( w->message().m_type == VKMessage::IncommingMessage )
	//	{
	//		margin.setLeft(size);
	//	}
	//	else
	//	{
	//		margin.setRight(size);
	//	}

	//	w->setContentsMargins(margin);
	//}

	QWidget::resizeEvent(event);
}

void VKTalkWidget::scrollToBottom()
{
    m_scrollArea->verticalScrollBar()->setValue(m_scrollArea->verticalScrollBar()->maximum());
}

void VKTalkWidget::initialize()
{
    m_scrollArea = new QScrollArea(this);
    m_scrollArea->setWidgetResizable(true);
    m_scrollAreaContentWidget = new QWidget();
    m_messagesLayout = new QVBoxLayout(m_scrollAreaContentWidget);
    m_messagesLayout->addStretch();
    m_scrollArea->setWidget(m_scrollAreaContentWidget);

    m_mainLayout = new QVBoxLayout;
    m_mainLayout->addWidget(m_scrollArea);

    m_inputMessageEdit = new VKQMInputMessageEdit;
    m_sendMessageButton = new QPushButton("Отправить");
	m_userBeginInput = new VKUserBeginInput(m_user->fname(), this);
	m_userBeginInput->setParent(this);

	m_sendMessageButton->setMinimumHeight(28);
    m_inputMessageEdit->setMaximumHeight(50);

    m_onlineLabel = new QLabel;
    m_onlineLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_onlineLabel->setAlignment(Qt::AlignHCenter);
    m_onlineLabel->setFixedWidth(50);
    m_onlineLabel->setStyleSheet("color: grey;");
    setUserOnline(m_user->isOnline(), m_user->onlineMobile());
	
    auto sendLayout = new QVBoxLayout;
    auto sendButtonLayout = new QHBoxLayout;
    sendButtonLayout->addWidget(m_sendMessageButton);
    sendButtonLayout->addWidget(m_userBeginInput);
    sendButtonLayout->addStretch();
    sendLayout->addWidget(m_inputMessageEdit, 0, Qt::AlignBottom);
    sendLayout->addLayout(sendButtonLayout);

    m_avatar = new QLabel;
    m_avatar->setFixedSize(50, 50);
    m_avatar->setPixmap(m_user->avatar());
    m_avatar->setStyleSheet("border-radius: 3px;");

    auto avaLayout = new QVBoxLayout;
    avaLayout->addWidget(m_avatar);
    avaLayout->addWidget(m_onlineLabel);

    auto layout = new QHBoxLayout;
    layout->addLayout(sendLayout);
    layout->addLayout(avaLayout);

    m_mainLayout->addLayout(layout);

    setLayout(m_mainLayout);

    m_scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    m_scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    connect(m_sendMessageButton, SIGNAL(clicked()),
            this, SLOT(sendMessage()));

    connect(m_inputMessageEdit, SIGNAL(readySendMessage()),
            m_sendMessageButton, SLOT(click()));

    /*! Seeting stylesheets !*/
    m_sendMessageButton->setObjectName("sendMessageButton");
    m_sendMessageButton->setStyleSheet("QPushButton { "
                                       "	font: 11px;"
                                       "	margin: 0 1px 0 1px;"
                                       "	color: white;"
                                       "	background-color: #6383a8;"
                                       "	border-style: outset;"
                                       "	border-radius: 2px;"
                                       "	border-width: 0px;"
                                       "    padding: 6px 16px 7px 16px;"
                                       "}"
                                       ""
                                       "QPushButton::pressed {"
                                       "	background-color: #6d8cb0;"
                                       "}"
                                       ""
                                       "QPushButton::disabled {"
                                       "    background-color: grey;"
                                       "}");

    m_scrollArea->setStyleSheet("QScrollArea { "
                                "   border: 0xp; "
                                "}"
                                "QScrollBar:vertical {               "
                                "   border: 0px solid #999999;"
                                "   background:white;"
                                "   width:10px;    "
                                "   margin: 0px 0px 0px 0px;"
                                "}"
                                "QScrollBar::handle:vertical {"
                                "   background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                "   stop: 0  #2b587a, stop: 0.5#2b587a,  stop:1 #2b587a);"
                                "   min-height: 0px;"
                                "}"
                                "    QScrollBar::add-line:vertical {"
                                "        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                "        stop: 0  #2b587a, stop: 0.5 #2b587a,  stop:1 #2b587a);"
                                "        height: px;"
                                "        subcontrol-position: bottom;"
                                "        subcontrol-origin: margin;"
                                "    }"
                                "    QScrollBar::sub-line:vertical {"
                                "        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                "        stop: 0  #2b587a, stop: 0.5 #2b587a,  stop:1 #2b587a);"
                                "        height: 0px;"
                                "        subcontrol-position: top;"
                                "        subcontrol-origin: margin;"
                                "    }"
                                "   "
                                "QScrollBar:horizontal {               "
                                "   border: 0px solid #999999;"
                                "   background:white;"
                                "   width:10px;    "
                                "   margin: 0px 0px 0px 0px;"
                                "}"
                                "QScrollBar::handle:horizontal {"
                                "   background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                "   stop: 0  #2b587a, stop: 0.5#2b587a,  stop:1 #2b587a);"
                                "   min-height: 0px;"
                                "}"
                                "    QScrollBar::add-line:horizontal {"
                                "        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                "        stop: 0  #2b587a, stop: 0.5 #2b587a,  stop:1 #2b587a);"
                                "        height: px;"
                                "        subcontrol-position: bottom;"
                                "        subcontrol-origin: margin;"
                                "    }"
                                "    QScrollBar::sub-line:horizontal {"
                                "        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                "        stop: 0  #2b587a, stop: 0.5 #2b587a,  stop:1 #2b587a);"
                                "        height: 0px;"
                                "        subcontrol-position: top;"
                                "        subcontrol-origin: margin;"
                                "    }"
                                );

    m_inputMessageEdit->setStyleSheet("QScrollBar:vertical {               "
                                      "   border: 0px solid #999999;"
                                      "   background:white;"
                                      "   width:10px;    "
                                      "   margin: 0px 0px 0px 0px;"
                                      "}"
                                      "QScrollBar::handle:vertical {"
                                      "   background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                      "   stop: 0  #2b587a, stop: 0.5#2b587a,  stop:1 #2b587a);"
                                      "   min-height: 0px;"
                                      "}"
                                      "    QScrollBar::add-line:vertical {"
                                      "        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                      "        stop: 0  #2b587a, stop: 0.5 #2b587a,  stop:1 #2b587a);"
                                      "        height: px;"
                                      "        subcontrol-position: bottom;"
                                      "        subcontrol-origin: margin;"
                                      "    }"
                                      "    QScrollBar::sub-line:vertical {"
                                      "        background: qlineargradient(x1:0, y1:0, x2:1, y2:0,"
                                      "        stop: 0  #2b587a, stop: 0.5 #2b587a,  stop:1 #2b587a);"
                                      "        height: 0px;"
                                      "        subcontrol-position: top;"
                                      "        subcontrol-origin: margin;"
                                      "    }"
                                      "   ");
}

//////////////////////////////////////////////////////////////////////////////////////////////

VKQMInputMessageEdit::VKQMInputMessageEdit(QWidget *parent)
    : QPlainTextEdit(parent)
{

}

void VKQMInputMessageEdit::keyPressEvent(QKeyEvent *e)
{
    int key = e->key();

	if( (key == Qt::Key_Return || key == Qt::Key_Enter) && e->modifiers() == Qt::NoModifier)
    {
        emit readySendMessage();
    }
    else if( (key == Qt::Key_Return || key == Qt::Key_Enter) && e->modifiers() == Qt::ControlModifier)
    {
        appendHtml("<br/>");
    }
    else
        QPlainTextEdit::keyPressEvent(e);
}

//////////////////////////////////////////////////////////////////////////////////////////////

VKUserBeginInput::VKUserBeginInput(QString userName, QWidget* parent /*= 0*/)
	: QWidget(parent)
{
	m_text = QString("%0 набирает сообщение...").arg(userName);
	m_movie = new QMovie(":/resources/icons/typing.gif", QByteArray(), this);
	m_timer = new QTimer(this);

	m_textLabel = new QLabel(m_text, this);
	m_movieLabel = new QLabel(this);
	m_textLabel->setStyleSheet("color: grey; font-size: 10px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;");
	m_movieLabel->setMovie(m_movie);

	connect(m_timer, &QTimer::timeout, this, &VKUserBeginInput::setUserEndInput);

	QHBoxLayout* layout = new QHBoxLayout;
	layout->addWidget(m_movieLabel);
	layout->addWidget(m_textLabel);
	setLayout(layout);

	this->setMaximumHeight(28);
	m_movieLabel->setMaximumHeight(26);
	m_textLabel->setMaximumHeight(28);
	QSize size = m_movie->scaledSize();
	size.setHeight(26);
	m_movie->setScaledSize(size);
	m_textLabel->setScaledContents(true);
	
	//m_movieLabel->hide();
	//m_textLabel->hide();

	setUserEndInput();
}

void VKUserBeginInput::setUserBeginInput()
{
	m_timer->start(5500);
	QMutexLocker lock(&m_mutex);
	m_textLabel->setText(m_text);
	m_movieLabel->setMovie(m_movie);
	m_movie->start();
}

void VKUserBeginInput::setUserEndInput()
{
	QMutexLocker lock(&m_mutex);
	m_movieLabel->clear();
	m_textLabel->clear();
	m_movie->stop();
}