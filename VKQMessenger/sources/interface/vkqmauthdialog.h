#ifndef VKQMAUTHDIALOG_H
#define VKQMAUTHDIALOG_H

#include <QDialog>

class VKQMAuthDialog : public QDialog
{
    Q_OBJECT
public:
    explicit VKQMAuthDialog(QWidget *parent = 0);

    static bool isAuthActive();
    bool authSucceed() { return m_success; }

private slots:
    void checkForToken(QUrl url);

private:
    bool m_success;

};

#endif // VKQMAUTHDIALOG_H
