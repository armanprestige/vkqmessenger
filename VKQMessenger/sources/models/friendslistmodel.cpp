#include "stdafx.h"
#include "network/vkconnector.h"
#include "friendslistmodel.h"

FriendsListModel::FriendsListModel(QObject *parent) :
    QAbstractListModel(parent)
{
}

int FriendsListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return VKConnector::getInstance().friendList().count();
}

QVariant FriendsListModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();

    if ( index.row() >= VKConnector::getInstance().friendList().count() )
        return QVariant();

    if(role == Qt::TextAlignmentRole)
        return Qt::AlignCenter;

    if(role == Qt::DisplayRole)
        return VKConnector::getInstance().friendList().at(index.row())->name();

    /*!
      * vkqm roles
      * */

    if(role == VKUserAvatarRole)
        return VKConnector::getInstance().friendList().at(index.row())->avatar();

    if(role == VKUserOnlineRole)
        return VKConnector::getInstance().friendList().at(index.row())->isOnline();

    if(role == VKUserIdRole)
        return VKConnector::getInstance().friendList().at(index.row())->id();

    if(role == VKUserRole)
    {
        QVariant var;
        var.setValue(VKConnector::getInstance().friendList().at(index.row()));
        return var;
    }

    return QVariant();
}

Qt::ItemFlags FriendsListModel::flags(const QModelIndex &index) const
{
    return QAbstractItemModel::flags(index);
}

void FriendsListModel::updateUserAvatar(QString id)
{
    int c = VKConnector::getInstance().friendList().count();
    for ( int i = 0; i < c; i++ )
    {
        if ( VKConnector::getInstance().friendList().at(i)->id() == id )
        {
            emit dataChanged(createIndex(i,0), createIndex(i,0));
            break;
        }
    }
}

void FriendsListModel::updateUserOnline(QString id)
{
    int c = VKConnector::getInstance().friendList().count();
    for ( int i = 0; i < c; i++ )
    {
        if ( VKConnector::getInstance().friendList().at(i)->id() == id )
        {
            emit dataChanged(createIndex(i,0), createIndex(i,0));
            break;
        }
    }
}

FriendsSortModel::FriendsSortModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
    m_onlyOnline = false;
    m_string.clear();
}

bool FriendsSortModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    return  (m_onlyOnline ? sourceModel()->index(source_row, 0, source_parent).data(VKUserOnlineRole).toBool() : true) &&
            sourceModel()->index(source_row, 0, source_parent).data(Qt::DisplayRole).toString().toLower().contains(m_string);
}

void FriendsSortModel::showOnlyOnline(bool b)
{
    m_onlyOnline = b;
}

void FriendsSortModel::setSearchString(QString str)
{
    m_string = str;
}

