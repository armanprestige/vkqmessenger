#include "stdafx.h"
#include "friendslistmodel.h"
#include "friendslistmodeldelegate.h"

FriendsListModelDelegate::FriendsListModelDelegate(QObject *parent) :
    QAbstractItemDelegate(parent)
{
}

void FriendsListModelDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QPen pen =  painter->pen();

    if(option.state & QStyle::State_Selected)
         painter->fillRect(option.rect, QColor("#94C6F7"));
     else if ( index.row() % 2 )
         painter->fillRect(option.rect, QColor("#E2EDF7"));

     QPixmap avatarPix = qvariant_cast<QPixmap>(index.data(VKUserAvatarRole));

     QRect r = option.rect.adjusted(0, 0, -0, -0);
     QIcon(avatarPix).paint(painter, r, Qt::AlignVCenter|Qt::AlignLeft);

     r = r.adjusted(r.height() + 10, 0, 0, 0);
     QString txt = index.data(Qt::DisplayRole).toString();

     if ( !index.data(VKUserOnlineRole).toBool() )
         painter->setPen(QColor(200, 200, 200));

     painter->drawText(r.left(), r.top(), r.width(), r.height(), Qt::AlignVCenter|Qt::AlignLeft|Qt::TextWordWrap, txt, &r);

     painter->setPen(pen);
}

QSize FriendsListModelDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);
    // Very dumb value. May be fix it in the future
    return QSize(250, 50);
}
