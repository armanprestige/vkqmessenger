#ifndef FRIENDSLISTMODELDELEGATE_H
#define FRIENDSLISTMODELDELEGATE_H

#include <QAbstractItemDelegate>

class FriendsListModelDelegate : public QAbstractItemDelegate
{
    Q_OBJECT
public:
    explicit FriendsListModelDelegate(QObject *parent = 0);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

signals:

public slots:

};

#endif // FRIENDSLISTMODELDELEGATE_H
