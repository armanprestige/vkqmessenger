#ifndef FRIENDSLISTMODEL_H
#define FRIENDSLISTMODEL_H

#include <QSortFilterProxyModel>
#include "vk/vkuser.h"

/*!
  * Roles for model
  */
#define VKUserRole          Qt::UserRole    + 1
#define VKUserOnlineRole    VKUserRole      + 1
#define VKUserAvatarRole    VKUserRole      + 2
#define VKUserIdRole        VKUserRole      + 3

/*!
 * \brief The FriendsListModel class
 */
class FriendsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit FriendsListModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent =  QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

signals:

public slots:
    void updateUserAvatar(QString id);
    void updateUserOnline(QString id);
};

class FriendsSortModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit FriendsSortModel(QObject *parent = 0);
//    ~FriendsSortModel();

    //bool lessThan(const QModelIndex &left, const QModelIndex &right) const;
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

    void showOnlyOnline(bool b);
    void setSearchString(QString str);

private:
    bool m_onlyOnline;
    QString m_string;
};

#endif // FRIENDSLISTMODEL_H
