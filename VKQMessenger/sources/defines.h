#ifndef DEFINES_H
#define DEFINES_H

#define DEVELOPER "Arman Oganesyan"
#define APPLICATION_NAME "Vk Quick Messenger"

#ifdef QT_DEBUG
#define MESSAGE_ICON "../trunk/resources/icons/message2_16x16.gif"
#else
#define MESSAGE_ICON "../trunk/resources/icons/message2_16x16.gif"
#endif

#ifdef __MINGW32__
#define NOEXCEPT noexcept
#else
#define NOEXCEPT noexcept
#endif

#endif // DEFINES_H
