#ifndef VKQMUTILS_H
#define VKQMUTILS_H

#include "network/requestsender.h"

namespace VKQMUtils
{
    bool mkpath(QString path);
    bool mkdir(QString dir);

    QByteArray doubleGet(Request &request, RequestSender &sender);
    QByteArray doublePost(Request &request, RequestSender &sender);

    QString fromUnixTimestamp(uint time, QString format = QString("dd.MM.yyyy в hh:mm:ss"));

    void playSound(QString path);
    QPixmap loadAvatar(QString url);

	template <typename T>
	void safeDelete(T** pointer_to_object)
	{
		if (nullptr != *pointer_to_object)
		{
			delete *pointer_to_object;
			*pointer_to_object = nullptr;
		}
	}
}

#endif // VKQMUTILS_H
