#include "stdafx.h"
#include "vkqmlogger.h"

//VKQMLogger *VKQMLogger::m_self = nullptr;

VKQMLogger::VKQMLogger()
{
}

VKQMLogger &VKQMLogger::getInstance()
{
    // This code is thread-safe becaouse new standard of C++ 11
    // initialize static variables without locking.
    static VKQMLogger self;
    return self;
}

void VKQMLogger::write(QString text, QString path)
{
    QMutexLocker locker(&m_mutex);
    QFile logFile(path);

    if ( logFile.open(QIODevice::WriteOnly | QIODevice::Append) )
    {
        QTextStream stream(&logFile);
         stream << QString("[" + QDate::currentDate().toString("dd.MM.yyyy") + " " +
                        QTime::currentTime().toString("HH:mm:ss")+"] " + text + ("\r\n"));
        logFile.close();
    }
}
