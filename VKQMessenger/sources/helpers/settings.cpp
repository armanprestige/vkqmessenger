#include "stdafx.h"
#include "defines.h"
#include "settings.h"

#define QSTR(a) QString(#a)

//// Initialize values by default
//// Note: param access_token coudn't be empty
//QString Settings::access_token = "access_token";
//QString Settings::user_name = "user_name";
//bool Settings::incomming_sound = true;
//int Settings::max_log_size = 5;
//bool Settings::stay_offline = false;

//Settings::Settings()
//{
//}

//void Settings::Save()
//{
//    QSettings settings(DEVELOPER, APPLICATION_NAME);
//    settings.setValue(QSTR(access_token), access_token);
//    settings.setValue(QSTR(incomming_sound), incomming_sound);
//    settings.setValue(QSTR(stay_offline), stay_offline);
//}

//void Settings::Load()
//{
//    QSettings settings(DEVELOPER, APPLICATION_NAME);
//    access_token    = settings.value(QSTR(access_token),    "access_token").toString();
//    incomming_sound = settings.value(QSTR(incomming_sound), true).toBool();
//    stay_offline    = settings.value(QSTR(stay_offline),    true).toBool();
//}

Settings::Settings()
{
    access_token = "access_token";
    user_name = "user_name";
    incomming_sound = true;
    max_log_size = 5;
    stay_offline = false;
    sort_friends_popular = false;
}

Settings &Settings::getInstance()
{
    // This code is thread-safe becaouse new standard of C++ 11
    // initialize static variables without locking.
    static Settings self;
    return self;
}

QVariant Settings::value(int valueId)
{
    QMutexLocker locker(&m_mutex);

    switch(valueId)
    {
    case SettingsValues::AccessToken: return access_token;
    case SettingsValues::IncommingSound: return incomming_sound;
    case SettingsValues::MaxLogSize: return max_log_size;
    case SettingsValues::StayOffline: return stay_offline;
    case SettingsValues::UserName: return user_name;
    case SettingsValues::SortFriendsPopular: return sort_friends_popular;
    case SettingsValues::ProxyAddress: return proxy_address;
    case SettingsValues::ProxyLogin: return proxy_login;
    case SettingsValues::ProxyPassword: return proxy_password;
    case SettingsValues::ProxyPort: return proxy_port;
    }

    return QVariant();
}

void Settings::setValue(int valueId, QVariant value)
{
    QMutexLocker locker(&m_mutex);

    switch(valueId)
    {
    case SettingsValues::AccessToken: access_token = value.toString(); break;
    case SettingsValues::IncommingSound: incomming_sound = value.toBool(); break;
    case SettingsValues::MaxLogSize: max_log_size = value.toInt(); break;
    case SettingsValues::StayOffline: stay_offline = value.toBool(); break;
    case SettingsValues::UserName: user_name = value.toString();
    case SettingsValues::SortFriendsPopular: sort_friends_popular = value.toBool(); break;
    case SettingsValues::ProxyAddress: proxy_address = value.toString(); break;
    case SettingsValues::ProxyLogin: proxy_login = value.toString(); break;;
    case SettingsValues::ProxyPassword: proxy_password = value.toString(); break;;
    case SettingsValues::ProxyPort: proxy_port = value.toString(); break;;
    }
}

void Settings::save()
{
    QMutexLocker locker(&m_mutex);
    //QSettings settings(DEVELOPER, APPLICATION_NAME);
    QSettings settings("vkqm.ini", QSettings::IniFormat);
    settings.setValue(QSTR(access_token), access_token);
    settings.setValue(QSTR(incomming_sound), incomming_sound);
    settings.setValue(QSTR(stay_offline), stay_offline);
    settings.setValue(QSTR(max_log_size), max_log_size);
    settings.setValue(QSTR(sort_friends_popular), sort_friends_popular);
    settings.setValue(QSTR(proxy_address), proxy_address);
    settings.setValue(QSTR(proxy_login), proxy_login);
    settings.setValue(QSTR(proxy_password), proxy_password);
    settings.setValue(QSTR(proxy_port), proxy_port);
}

void Settings::load()
{
    QMutexLocker locker(&m_mutex);
    //QSettings settings(DEVELOPER, APPLICATION_NAME);
    QSettings settings("vkqm.ini", QSettings::IniFormat);
    access_token        = settings.value(QSTR(access_token),            "0x000000").toString();
    incomming_sound     = settings.value(QSTR(incomming_sound),         true).toBool();
    stay_offline        = settings.value(QSTR(stay_offline),            false).toBool();
    max_log_size        = settings.value(QSTR(max_log_size),            5).toInt();
    sort_friends_popular= settings.value(QSTR(sort_friends_popular),    true).toBool();
    proxy_address       = settings.value(QSTR(proxy_address),           "").toString();
    proxy_login         = settings.value(QSTR(proxy_login),             "").toString();
    proxy_password      = settings.value(QSTR(proxy_password),          "").toString();
    proxy_port          = settings.value(QSTR(proxy_port),              "").toString();

    QNetworkProxy::setApplicationProxy(
                proxy_address.isEmpty() ?
                QNetworkProxy()
                  :
                QNetworkProxy(QNetworkProxy::HttpProxy,
                              Settings::getInstance().value(SettingsValues::ProxyAddress).toString(),
                              Settings::getInstance().value(SettingsValues::ProxyPort).toInt(),
                              Settings::getInstance().value(SettingsValues::ProxyLogin).toString(),
                              Settings::getInstance().value(SettingsValues::ProxyPassword).toString()
                              )
                );
}
