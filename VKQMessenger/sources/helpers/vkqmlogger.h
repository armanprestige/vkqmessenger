#ifndef VKQMLOGGER_H
#define VKQMLOGGER_H

/*!
 * \brief The VKQMLogger class
 * This is thread safe logger
 * Created by: Arman Oganesyan
 */

class VKQMLogger
{
//public:	
//	enum OutputDeviceType {
//		Console,
//		File,
//		FileAndConsole
//	};
//
public:
    static VKQMLogger& getInstance();
    void write(QString text, QString path = QString("vkqm.log"));
	//void setDeviceType(OutputDeviceType type);


private:
    VKQMLogger();
    VKQMLogger(const VKQMLogger&);
    VKQMLogger& operator= (const VKQMLogger&);
    VKQMLogger& operator= (const VKQMLogger&&);

    QMutex m_mutex;
    //static VKQMLogger *m_self;

};

#endif // VKQMLOGGER_H
