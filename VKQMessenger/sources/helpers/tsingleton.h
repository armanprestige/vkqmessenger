#ifndef TSINGLETON_H
#define TSINGLETON_H

#include "vkqmdisablecopy.h"

template <class T>
class TSingleton
{
public:
    static T* getInstance()
    {
        if ( nullptr == m_instance)
            m_instance = new T;
        return m_instance;
    }

protected:
    TSingleton();
    ~TSingleton();

private:
    TSingleton(const TSingleton&);
    TSingleton& operator= (const TSingleton&);
    TSingleton& operator= (const TSingleton&&);

    static T* m_instance;
};

template <class T>
T* TSingleton<T>::m_instance = nullptr;

#endif // TSINGLETON_H
