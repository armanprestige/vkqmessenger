#ifndef VKQMDISABLECOPY_H
#define VKQMDISABLECOPY_H

#ifndef VKQM_DISABLE_COPY
#define VKQM_DISABLE_COPY(Class)\
    Class(const Class &);\
    Class& operator=(const Class & );\
    Class& operator=(const Class &&);
#endif

#endif // VKQMDISABLECOPY_H
