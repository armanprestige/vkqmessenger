#ifndef SETTINGS_H
#define SETTINGS_H

#include "vkqmdisablecopy.h"


namespace SettingsValues
{
    enum {
        AccessToken,
        UserName,
        IncommingSound,
        MaxLogSize,
        StayOffline,
        SortFriendsPopular,
        ProxyAddress,
        ProxyPort,
        ProxyLogin,
        ProxyPassword
    };
}

/*!
 * \brief The Settings class
 * This is thread safe settings class
 */
class Settings
{
public:
    Settings();
    static Settings& getInstance();

    QVariant value(int valueId);
    void setValue(int valueId, QVariant value);

    void save();
    void load();

private:
    VKQM_DISABLE_COPY(Settings)

    QMutex m_mutex;

    QVariantMap m_values;

    QString access_token;
    QString user_name;
    bool incomming_sound;
    int max_log_size;
    bool stay_offline;
    bool sort_friends_popular;
    QString proxy_address;
    QString proxy_port;
    QString proxy_login;
    QString proxy_password;

};

#endif // SETTINGS_H
