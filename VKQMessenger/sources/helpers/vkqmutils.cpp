#include "stdafx.h"
#include "vkqmutils.h"

bool VKQMUtils::mkpath(QString path)
{
    QDir qdir;
    return qdir.mkpath(path);
}


bool VKQMUtils::mkdir(QString dir)
{
    QDir qdir;
    return qdir.mkdir(dir);
}

QByteArray VKQMUtils::doubleGet(Request &request, RequestSender &sender)
{
    QByteArray temp = sender.get(request);
    if ( temp.isEmpty() )
        temp = sender.get(request);
    return temp;
}

QByteArray VKQMUtils::doublePost(Request &request, RequestSender &sender)
{
    QByteArray temp = sender.post(request);
    if ( temp.isEmpty() )
        temp = sender.post(request);
    return temp;
}

QString VKQMUtils::fromUnixTimestamp(uint time, QString format)
{
    QDateTime t_stamp;
    t_stamp.setTime_t(time);
    return t_stamp.toString(format);
}

void VKQMUtils::playSound(QString path)
{
    QMediaPlayer *player = new QMediaPlayer;
    player->setMedia(QUrl::fromLocalFile(path));

    QObject::connect(player, &QMediaPlayer::stateChanged, [=](QMediaPlayer::State state)
    {
        if (state == QMediaPlayer::StoppedState)
            player->deleteLater();
    });

    player->play();
}


QPixmap VKQMUtils::loadAvatar(QString url)
{
    if ( !url.contains("/") ) {
        WRITE_TRACE("VKQMUtils::loadAvatar !url.contains(\"/\")");
        return QPixmap();
    }

    QString fname = url.split("/").last();
    QString fpath = "avatars/" + fname;

    QFile file(fpath);
    if ( file.exists() && file.size() )
        return QPixmap(fpath);

    RequestSender sender;
    QByteArray icon = sender.get(url);
    VKQMUtils::mkdir("avatars");

    if ( file.open(QIODevice::WriteOnly) )
    {
        file.write(icon);
        file.close();
        return QPixmap(fpath);
    }
    else
        WRITE_TRACE("VKQMUtils::loadAvatar !file.open(QIODevice::WriteOnly) [TRY TO START APP WITH ADMIN RIGHTS!");

    return QPixmap();
}
