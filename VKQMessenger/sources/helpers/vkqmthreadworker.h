#ifndef VKQMTHREADWORKER_H
#define VKQMTHREADWORKER_H

/*!
 * \brief The VKQMThreadWorker class
 * Description: This class provide interface for
 * fast creating of objects, that have to work in another thread
 * For using this class you have to derive this
 * Created by: Arman Oganesyan
 */
class VKQMThreadWorker : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief VKQMThreadWorker
     * \param parent
     */
    explicit VKQMThreadWorker(QObject *parent = 0);

    virtual ~VKQMThreadWorker();

    /*!
     * \brief run your worker class at another thread
     * \param worker - pointer to you worker class
     */
    static void startWorkerThread(VKQMThreadWorker *worker);

public slots:
    /*!
     * \brief run - reimplement this function - add worker code only
     * in this pure virtual method
     */
    virtual void run() = 0;

    /*!
     * \brief stop - terminate working and delete object & thread
     */
    void stop();

signals:
    /*!
     * \brief finished - emited when work is finished
     */
    void finished();

protected:
    /*!
     * \brief m_isWorking
     */
    std::atomic_bool m_isWorking;
};

/*!
 * \brief Worker is typedef for VKQMThreadWorker for fast typing
 */
typedef VKQMThreadWorker Worker;

#endif // VKQMTHREADWORKER_H
