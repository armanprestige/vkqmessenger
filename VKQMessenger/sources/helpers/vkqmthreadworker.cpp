#include "stdafx.h"
#include "vkqmthreadworker.h"

VKQMThreadWorker::VKQMThreadWorker(QObject *parent)
    : QObject(parent)
{
    m_isWorking = false;
}

VKQMThreadWorker::~VKQMThreadWorker()
{
#ifdef QT_DEBUG
    qDebug() << "~VKQMThreadWorker";
#endif
}

/*static*/void VKQMThreadWorker::startWorkerThread(VKQMThreadWorker *worker)
{
    QThread* thread = new QThread();

    worker->moveToThread(thread);

    connect(worker, &Worker::finished, thread, &QThread::quit);
    connect(worker, &Worker::finished, worker, &Worker::deleteLater);
    connect(thread, &QThread::started, worker, &Worker::run);
    connect(thread, &QThread::finished, thread, &QThread::deleteLater);

    thread->start();
}

void VKQMThreadWorker::stop()
{
    m_isWorking = false;
}
